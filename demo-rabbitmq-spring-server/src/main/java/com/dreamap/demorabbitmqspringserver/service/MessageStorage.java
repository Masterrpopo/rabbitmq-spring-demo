package com.dreamap.demorabbitmqspringserver.service;

import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Service
public class MessageStorage {
    private final List<String> messages;

    public MessageStorage() {
        this.messages = Collections.synchronizedList(new LinkedList<>());
    }

    public void storeMessage(String message) {
        messages.add(message);
    }

    public List<String> retrieveMessages() {
        return List.copyOf(messages);
    }
}
