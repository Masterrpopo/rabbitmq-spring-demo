package com.dreamap.demorabbitmqspringserver.service;

import com.dreamap.commons.dto.PossiblePositiveParts;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class Calculator {
    public double sum(double value1, double value2) {
        return value1 + value2;
    }

    public long factorial(int base) {
        if (base < 0) {
            throw new IllegalArgumentException("Argument must be greater than or equal to 0");
        } else if (base == 0) {
            return 1L;
        } else {
            long factorial = 1;
            for (int i = 1; i <= base; i++) {
                factorial = factorial * i;
            }
            return factorial;
        }

    }

    public List<PossiblePositiveParts> possiblePositiveParts(Integer number) {
        final List<PossiblePositiveParts> possiblePositivePartsList = new ArrayList<>();

        for (int i = 0; i <= number; i++) {
            possiblePositivePartsList.add(new PossiblePositiveParts(i, number - i));
        }

        return possiblePositivePartsList;
    }
}
