package com.dreamap.demorabbitmqspringserver.config;

import com.dreamap.rabbitmqerrorhandler.CustomRabbitListenerErrorHandler;
import com.dreamap.rabbitmqerrorhandler.factory.queueresponse.ErrorQueueResponseFactory;
import org.springframework.amqp.rabbit.listener.api.RabbitListenerErrorHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
@ComponentScan(basePackages = {"com.dreamap.rabbitmqerrorhandler.factory", "com.dreamap.commons.factory"})
public class DemoAmqpHandlersConfig {

    @Bean
    public RabbitListenerErrorHandler customRabbitListenerErrorHandler(List<ErrorQueueResponseFactory> queueResponseFactories) {
        return new CustomRabbitListenerErrorHandler(queueResponseFactories);
    }

}
