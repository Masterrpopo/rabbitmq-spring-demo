package com.dreamap.demorabbitmqspringserver.config;

import com.dreamap.commons.config.amqp.CalculationAmqpConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class DemoCalculationAmqpConfig {
    private final CalculationAmqpConfig config;

    @Bean
    public DirectExchange calculationExchange() {
        return new DirectExchange(config.getCalculationExchangeName());
    }

    @Bean
    public Queue sumCalculationQueue() {
        return new Queue(config.getSumCalculationQueueName());
    }

    @Bean
    public Binding sumCalculationBinding(DirectExchange calculationExchange, Queue sumCalculationQueue) {
        return BindingBuilder.bind(sumCalculationQueue).to(calculationExchange).withQueueName();
    }

    @Bean
    public Queue factorialCalculationQueue() {
        return new Queue(config.getFactorialCalculationQueueName());
    }

    @Bean
    public Binding factorialCalculationBinding(DirectExchange calculationExchange, Queue factorialCalculationQueue) {
        return BindingBuilder.bind(factorialCalculationQueue).to(calculationExchange).withQueueName();
    }

    @Bean
    public Queue possiblePositivePartsQueue() {
        return new Queue(config.getPossiblePositivePartsQueueName());
    }

    @Bean
    public Binding possiblePositivePartsBinding(DirectExchange calculationExchange, Queue possiblePositivePartsQueue) {
        return BindingBuilder.bind(possiblePositivePartsQueue).to(calculationExchange).withQueueName();
    }

}
