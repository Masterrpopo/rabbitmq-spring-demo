package com.dreamap.demorabbitmqspringserver.config;

import com.dreamap.commons.config.amqp.MessageStorageAmqpConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class DemoMessageStorageAmqpConfig {
    private final MessageStorageAmqpConfig config;

    @Bean
    public DirectExchange messageStorageExchange() {
        return new DirectExchange(config.getMessageStorageExchangeName());
    }

    @Bean
    public Queue storeMessageQueue() {
        return new Queue(config.getStoreMessageQueueName());
    }

    @Bean
    public Binding storeMessageBinding(DirectExchange messageStorageExchange, Queue storeMessageQueue) {
        return BindingBuilder.bind(storeMessageQueue).to(messageStorageExchange).withQueueName();
    }

    @Bean
    public Queue retrieveMessagesQueue() {
        return new Queue(config.getRetrieveMessagesQueueName());
    }

    @Bean
    public Binding retrieveMessagesBinding(DirectExchange messageStorageExchange, Queue retrieveMessagesQueue) {
        return BindingBuilder.bind(retrieveMessagesQueue).to(messageStorageExchange).withQueueName();
    }
}
