package com.dreamap.demorabbitmqspringserver.config;

import com.dreamap.commons.config.amqp.ErrorAmqpConfig;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class DemoErrorHandlingAmqpConfig {
    private final ErrorAmqpConfig config;

    @Bean
    public TopicExchange errorExchange() {
        return new TopicExchange(config.getErrorExchangeName());
    }

    @Bean
    public Queue timeoutQueue() {
        return new Queue(config.getTimeoutQueueName());
    }

    @Bean
    public Binding timeoutBinding(TopicExchange errorExchange, Queue timeoutQueue) {
        return BindingBuilder.bind(timeoutQueue).to(errorExchange).with(config.getTimeoutRouting());
    }

    @Bean
    public Queue customTimeoutQueue() {
        return new Queue(config.getCustomTimeoutQueueName());
    }

    @Bean
    public Binding customTimeoutBinding(TopicExchange errorExchange, Queue customTimeoutQueue) {
        return BindingBuilder.bind(customTimeoutQueue).to(errorExchange).with(config.getCustomTimeoutRouting());
    }

    @Bean
    public Queue illegalStateQueue() {
        return new Queue(config.getIllegalStateQueueName());
    }

    @Bean
    public Binding illegalStateBinding(TopicExchange errorExchange, Queue illegalStateQueue) {
        return BindingBuilder.bind(illegalStateQueue).to(errorExchange).with(config.getIllegalStateRouting());
    }

    @Bean
    public Queue httpStatusQueue() {
        return new Queue(config.getHttpStatusQueueName());
    }

    @Bean
    public Binding httpStatusBinding(TopicExchange errorExchange, Queue httpStatusQueue) {
        return BindingBuilder.bind(httpStatusQueue).to(errorExchange).with(config.getHttpStatusRouting());
    }

    @Bean
    public Queue customExceptionQueue() {
        return new Queue(config.getCustomExceptionQueueName());
    }

    @Bean
    public Binding customExceptionBinding(TopicExchange errorExchange, Queue customExceptionQueue) {
        return BindingBuilder.bind(customExceptionQueue).to(errorExchange).with(config.getCustomExceptionRouting());
    }

    @Bean
    public Queue customQueueExceptionQueue() {
        return new Queue(config.getCustomQueueExceptionQueueName());
    }

    @Bean
    public Binding customQueueExceptionBinding(TopicExchange errorExchange, Queue customQueueExceptionQueue) {
        return BindingBuilder.bind(customQueueExceptionQueue).to(errorExchange).with(config.getCustomQueueExceptionRouting());
    }
}
