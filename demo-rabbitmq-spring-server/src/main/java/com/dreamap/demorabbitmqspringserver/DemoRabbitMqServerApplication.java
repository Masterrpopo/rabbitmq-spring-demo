package com.dreamap.demorabbitmqspringserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoRabbitMqServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoRabbitMqServerApplication.class, args);
    }

}
