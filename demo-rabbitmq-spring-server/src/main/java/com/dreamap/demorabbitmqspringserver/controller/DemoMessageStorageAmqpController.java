package com.dreamap.demorabbitmqspringserver.controller;

import com.dreamap.commons.dto.response.MessagesResponse;
import com.dreamap.demorabbitmqspringserver.service.MessageStorage;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DemoMessageStorageAmqpController {
    private final MessageStorage messageStorage;

    @RabbitListener(queues = "${exchange.messageStorage.queue.store.name}", errorHandler = "customRabbitListenerErrorHandler")
    public void storeMessage(String message) {
        messageStorage.storeMessage(message);
    }

    @RabbitListener(queues = "${exchange.messageStorage.queue.retrieve.name}", errorHandler = "customRabbitListenerErrorHandler")
    public MessagesResponse retrieveMessages() {
        final List<String> messages = messageStorage.retrieveMessages();
        return new MessagesResponse(messages);
    }
}
