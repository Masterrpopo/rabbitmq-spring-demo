package com.dreamap.demorabbitmqspringserver.controller;

import com.dreamap.amqp.model.ex.TimeoutQueueException;
import com.dreamap.commons.ex.DemoAccessException;
import com.dreamap.commons.ex.queue.DemoCustomQueueException;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;

import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
public class DemoErrorAmqpController {

    @RabbitListener(queues = "${exchange.error.queue.timeout.name}", errorHandler = "customRabbitListenerErrorHandler")
    public String timeout() throws InterruptedException {
        final Duration duration = Duration.ofSeconds(10);
        Thread.sleep(duration.toMillis());
        return "Example string sent back after %s".formatted(duration);
    }

    @RabbitListener(queues = "${exchange.error.queue.customTimeout.name}", errorHandler = "customRabbitListenerErrorHandler")
    public Long customTimeout() throws TimeoutQueueException {
        throw new TimeoutQueueException("Custom timeout exception thrown as example");
    }

    @RabbitListener(queues = "${exchange.error.queue.illegalState.name}", errorHandler = "customRabbitListenerErrorHandler")
    public Integer illegalState() {
        throw new IllegalStateException("IllegalStateException thrown as example");
    }

    @RabbitListener(queues = "${exchange.error.queue.httpStatus.name}", errorHandler = "customRabbitListenerErrorHandler")
    public List<String> httpStatus() {
        throw new HttpServerErrorException(HttpStatus.CONFLICT, "HttpServerErrorException thrown as example");
    }

    @RabbitListener(queues = "${exchange.error.queue.customException.name}", errorHandler = "customRabbitListenerErrorHandler")
    public Set<Integer> customException() throws DemoAccessException {
        throw new DemoAccessException("DemoAccessException thrown as example", "user123");
    }

    @RabbitListener(queues = "${exchange.error.queue.customQueueException.name}", errorHandler = "customRabbitListenerErrorHandler")
    public Map<Integer, String> customQueueException() throws DemoCustomQueueException {
        throw new DemoCustomQueueException("DemoCustomQueueException thrown as example", "Some extra info");
    }
}
