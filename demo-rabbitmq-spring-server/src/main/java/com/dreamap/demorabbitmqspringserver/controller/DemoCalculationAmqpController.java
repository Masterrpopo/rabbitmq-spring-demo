package com.dreamap.demorabbitmqspringserver.controller;

import com.dreamap.commons.dto.PossiblePositiveParts;
import com.dreamap.commons.dto.request.CalculateSumRequest;
import com.dreamap.commons.dto.response.PossiblePositivePartsResponse;
import com.dreamap.demorabbitmqspringserver.service.Calculator;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class DemoCalculationAmqpController {
    private final Calculator calculator;

    @RabbitListener(queues = "${exchange.calculation.queue.sum.name}", errorHandler = "customRabbitListenerErrorHandler")
    public Double sumCalculation(CalculateSumRequest request) {
        return calculator.sum(request.getNum1(), request.getNum2());
    }

    @RabbitListener(queues = "${exchange.calculation.queue.factorial.name}", errorHandler = "customRabbitListenerErrorHandler")
    public Long factorialCalculation(Integer base) {
        return calculator.factorial(base);
    }

    @RabbitListener(queues = "${exchange.calculation.queue.possiblePositiveParts.name}", errorHandler = "customRabbitListenerErrorHandler")
    public PossiblePositivePartsResponse possiblePositiveParts(Integer number) {
        List<PossiblePositiveParts> possiblePositivePartsList = calculator.possiblePositiveParts(number);
        return new PossiblePositivePartsResponse(possiblePositivePartsList);
    }
}
