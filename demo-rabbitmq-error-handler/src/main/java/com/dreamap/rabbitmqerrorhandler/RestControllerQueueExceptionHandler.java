package com.dreamap.rabbitmqerrorhandler;

import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.rabbitmqerrorhandler.factory.rest.QueueExceptionResponseEntityFactory;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Comparator;
import java.util.List;

@Log4j2
@ControllerAdvice
@AllArgsConstructor
public class RestControllerQueueExceptionHandler {
    private final List<QueueExceptionResponseEntityFactory<?>> responseEntityFactories;

    @ExceptionHandler(QueueException.class)
    public ResponseEntity<?> handleException(QueueException queueException) {
        final QueueExceptionResponseEntityFactory<?> queueResponseFactory = getMatchingFactory(queueException);
        final ResponseEntity<?> errorQueueResponseResponseEntity = queueResponseFactory.buildResponseEntity(queueException);

        log.error("Catching %s and building ResponseEntity<%s>".formatted(queueException.getClass().getSimpleName(), queueResponseFactory.getBodyType().getSimpleName()), queueException);
        return errorQueueResponseResponseEntity;
    }

    private QueueExceptionResponseEntityFactory<?> getMatchingFactory(QueueException queueException) {
        return responseEntityFactories.stream()
                .filter(factory -> factory.supportsType(queueException))
                .min(Comparator.comparingInt(factory -> factory.getDistance(queueException)))
                .orElseThrow(() -> new IllegalStateException("Could not find matching QueueExceptionResponseEntityFactory"));
    }
}
