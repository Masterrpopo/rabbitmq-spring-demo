package com.dreamap.rabbitmqerrorhandler.factory.ex;

import com.dreamap.amqp.model.ex.InternalQueueException;
import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import org.springframework.stereotype.Component;

@Component
public class FallbackQueueExceptionFactory extends AbstractQueueExceptionFactory {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return ErrorQueueResponse.class.isAssignableFrom(cls);
    }

    @Override
    public QueueException buildException(ErrorQueueResponse errorQueueResponse) {
        return new InternalQueueException(errorQueueResponse.getMessage());
    }
}
