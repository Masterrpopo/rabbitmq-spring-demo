package com.dreamap.rabbitmqerrorhandler.factory.ex;

import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.amqp.model.ex.TimeoutQueueException;
import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import com.dreamap.amqp.model.queueresponse.TimeoutExceptionQueueResponse;
import org.springframework.stereotype.Component;

@Component
public class TimeoutQueueExceptionFactory extends AbstractQueueExceptionFactory {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return TimeoutExceptionQueueResponse.class.isAssignableFrom(cls);
    }

    @Override
    public QueueException buildException(ErrorQueueResponse errorQueueResponse) {
        return new TimeoutQueueException(errorQueueResponse.getMessage());
    }
}
