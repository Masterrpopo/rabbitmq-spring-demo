package com.dreamap.rabbitmqerrorhandler.factory.rest;

import com.dreamap.amqp.model.ex.QueueException;
import org.springframework.http.ResponseEntity;

/**
 * @param <T> ResponseEntity's body type
 */
public interface QueueExceptionResponseEntityFactory<T> {
    boolean supportsType(QueueException queueException);

    /**
     * @return Amount of subclasses that occurred between supported and given types
     */
    int getDistance(QueueException queueException);

    ResponseEntity<T> buildResponseEntity(QueueException queueException);

    Class<? extends T> getBodyType();
}
