package com.dreamap.rabbitmqerrorhandler.factory.ex;

import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;

public interface QueueExceptionFactory {
    boolean supportsType(ErrorQueueResponse errorQueueResponse);

    /**
     * @return Amount of subclasses that occurred between supported and given types
     */
    int getDistance(ErrorQueueResponse errorQueueResponse);

    QueueException buildException(ErrorQueueResponse errorQueueResponse);
}
