package com.dreamap.rabbitmqerrorhandler.factory.queueresponse;

public abstract class AbstractErrorQueueResponseFactory implements ErrorQueueResponseFactory {
    @Override
    public boolean supportsType(Throwable throwable) {
        return supportsType(throwable.getClass());
    }

    @Override
    public int getDistance(Throwable throwable) {
        if (!supportsType(throwable.getClass())) {
            throw buildUnsupportedTypeException(throwable);
        } else {
            int ancestors = 0;
            Class<?> superclass = throwable.getClass().getSuperclass();

            while (superclass != null && supportsType(superclass)) {
                superclass = superclass.getSuperclass();
                ancestors++;
            }

            return ancestors;
        }
    }

    protected RuntimeException buildUnsupportedTypeException(Throwable throwable) {
        return new IllegalStateException("Given input is not of supported type", throwable);
    }

    protected abstract boolean supportsType(Class<?> cls);

}
