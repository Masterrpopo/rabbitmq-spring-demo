package com.dreamap.rabbitmqerrorhandler.factory.ex;

import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;

public abstract class AbstractQueueExceptionFactory implements QueueExceptionFactory {

    @Override
    public boolean supportsType(ErrorQueueResponse errorQueueResponse) {
        return supportsType(errorQueueResponse.getClass());
    }

    @Override
    public int getDistance(ErrorQueueResponse errorQueueResponse) {
        if (!supportsType(errorQueueResponse)) {
            throw buildUnsupportedTypeException(errorQueueResponse);
        } else {
            int ancestors = 0;
            Class<?> superclass = errorQueueResponse.getClass().getSuperclass();

            while (superclass != null && supportsType(superclass)) {
                superclass = superclass.getSuperclass();
                ancestors++;
            }

            return ancestors;
        }
    }

    protected RuntimeException buildUnsupportedTypeException(ErrorQueueResponse errorQueueResponse) {
        return new IllegalStateException("Given input '%s' is not of supported type".formatted(errorQueueResponse.getClass()));
    }

    protected abstract boolean supportsType(Class<?> cls);
}
