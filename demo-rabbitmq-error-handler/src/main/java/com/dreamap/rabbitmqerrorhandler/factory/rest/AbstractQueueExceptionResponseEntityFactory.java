package com.dreamap.rabbitmqerrorhandler.factory.rest;

import com.dreamap.amqp.model.ex.QueueException;

public abstract class AbstractQueueExceptionResponseEntityFactory<T> implements QueueExceptionResponseEntityFactory<T> {

    @Override
    public boolean supportsType(QueueException queueException) {
        return supportsType(queueException.getClass());
    }

    @Override
    public int getDistance(QueueException queueException) {
        if (!supportsType(queueException)) {
            throw buildUnsupportedTypeException(queueException);
        } else {
            int ancestors = 0;
            Class<?> superclass = queueException.getClass().getSuperclass();

            while (superclass != null && supportsType(superclass)) {
                superclass = superclass.getSuperclass();
                ancestors++;
            }

            return ancestors;
        }
    }

    protected RuntimeException buildUnsupportedTypeException(QueueException queueException) {
        return new IllegalStateException("Given input is not of supported type", queueException);
    }

    protected abstract boolean supportsType(Class<?> cls);
}
