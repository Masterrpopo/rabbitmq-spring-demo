package com.dreamap.rabbitmqerrorhandler.factory.queueresponse;

import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import com.dreamap.amqp.model.queueresponse.InternalExceptionQueueResponse;
import org.springframework.stereotype.Component;

@Component
public class FallbackQueueResponseFactory extends AbstractErrorQueueResponseFactory {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return Throwable.class.isAssignableFrom(cls);
    }

    @Override
    public ErrorQueueResponse buildErrorQueueResponse(Throwable throwable) {
        return new InternalExceptionQueueResponse(throwable.getMessage());
    }
}
