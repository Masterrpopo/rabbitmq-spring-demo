package com.dreamap.rabbitmqerrorhandler.factory.queueresponse;

import com.dreamap.amqp.model.ex.TimeoutQueueException;
import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import com.dreamap.amqp.model.queueresponse.TimeoutExceptionQueueResponse;
import org.springframework.stereotype.Component;

@Component
public class TimeoutQueueResponseFactory extends AbstractErrorQueueResponseFactory {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return TimeoutQueueException.class.isAssignableFrom(cls);
    }

    @Override
    public ErrorQueueResponse buildErrorQueueResponse(Throwable throwable) {
        return new TimeoutExceptionQueueResponse(throwable.getMessage());
    }
}
