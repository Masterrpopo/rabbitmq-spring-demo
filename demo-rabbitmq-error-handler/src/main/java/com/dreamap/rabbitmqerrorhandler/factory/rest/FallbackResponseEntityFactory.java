package com.dreamap.rabbitmqerrorhandler.factory.rest;

import com.dreamap.amqp.model.ex.QueueException;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class FallbackResponseEntityFactory extends AbstractQueueExceptionResponseEntityFactory<String> {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return QueueException.class.isAssignableFrom(cls);
    }

    @Override
    public ResponseEntity<String> buildResponseEntity(QueueException input) {
        return ResponseEntity.internalServerError().body(input.getMessage());
    }

    @Override
    public Class<? extends String> getBodyType() {
        return String.class;
    }
}
