package com.dreamap.rabbitmqerrorhandler.factory.queueresponse;

import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;

public interface ErrorQueueResponseFactory {
    boolean supportsType(Throwable throwable);

    /**
     * @return Amount of subclasses that occurred between supported and given types
     */
    int getDistance(Throwable throwable);

    ErrorQueueResponse buildErrorQueueResponse(Throwable throwable);
}
