package com.dreamap.rabbitmqerrorhandler;

import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import com.dreamap.rabbitmqerrorhandler.factory.queueresponse.ErrorQueueResponseFactory;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.listener.api.RabbitListenerErrorHandler;
import org.springframework.amqp.rabbit.support.ListenerExecutionFailedException;

import java.util.Comparator;
import java.util.List;

@Log4j2
@RequiredArgsConstructor
public class CustomRabbitListenerErrorHandler implements RabbitListenerErrorHandler {
    private final List<ErrorQueueResponseFactory> queueResponseFactories;

    @Override
    public Object handleError(Message amqpMessage, org.springframework.messaging.Message<?> message, ListenerExecutionFailedException exception) {
        final Throwable errorCause = exception.getCause();
        final ErrorQueueResponseFactory errorQueueResponseFactory = getMatchingFactory(errorCause);
        final ErrorQueueResponse queueResponse = errorQueueResponseFactory.buildErrorQueueResponse(errorCause);

        log.error("Catching %s and mapping into %s".formatted(errorCause.getClass().getSimpleName(), queueResponse.getClass().getSimpleName()), errorCause);
        return queueResponse;
    }

    private ErrorQueueResponseFactory getMatchingFactory(Throwable throwable) {
        return queueResponseFactories.stream()
                .filter(factory -> factory.supportsType(throwable))
                .min(Comparator.comparingInt(factory -> factory.getDistance(throwable)))
                .orElseThrow(() -> new IllegalStateException("Could not find matching ErrorQueueResponseFactory"));
    }
}
