package com.dreamap.rabbitmqerrorhandler;

import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import com.dreamap.rabbitmqerrorhandler.factory.ex.QueueExceptionFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;

import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Log4j2
@RequiredArgsConstructor
public class QueueExceptionMessagePostProcessor implements MessagePostProcessor {
    private final List<QueueExceptionFactory> queueExceptionFactories;
    private final ObjectMapper objectMapper;

    @Override
    public Message postProcessMessage(Message message) throws AmqpException {
        final Optional<ErrorQueueResponse> errorQueueResponse = deserializeToErrorQueueResponse(message.getBody());

        if (errorQueueResponse.isPresent()) {
            throw buildException(errorQueueResponse.get());
        } else {
            return message;
        }
    }

    private Optional<ErrorQueueResponse> deserializeToErrorQueueResponse(byte[] rawMessage) {
        try {
            return Optional.ofNullable(objectMapper.readValue(rawMessage, ErrorQueueResponse.class));
        } catch (IOException ex) {
            return Optional.empty();
        }
    }

    private QueueException buildException(ErrorQueueResponse errorQueueResponse) {
        final QueueExceptionFactory exceptionFactory = getMatchingFactory(errorQueueResponse);
        final QueueException queueException = exceptionFactory.buildException(errorQueueResponse);

        log.error("Received {}, mapping and throwing {}", errorQueueResponse.getClass().getSimpleName(), queueException.getClass().getSimpleName());
        return queueException;
    }

    private QueueExceptionFactory getMatchingFactory(ErrorQueueResponse errorResponse) {
        return queueExceptionFactories.stream()
                .filter(factory -> factory.supportsType(errorResponse))
                .min(Comparator.comparingInt(factory -> factory.getDistance(errorResponse)))
                .orElseThrow(() -> new IllegalStateException("Could not find matching QueueExceptionFactory"));
    }
}
