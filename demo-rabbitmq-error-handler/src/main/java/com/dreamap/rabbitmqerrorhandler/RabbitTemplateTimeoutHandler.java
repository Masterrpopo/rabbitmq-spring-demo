package com.dreamap.rabbitmqerrorhandler;

import com.dreamap.amqp.model.ex.TimeoutQueueException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

import java.time.Duration;

@Log4j2
@Aspect
@RequiredArgsConstructor
public class RabbitTemplateTimeoutHandler {
    private final Duration rabbitTemplateReplyTimeout;
    private final Duration rabbitTemplateReceiveTimeout;

    @Pointcut("execution(* org.springframework.amqp.rabbit.core.RabbitTemplate.convertSendAndReceiveAsType(..))")
    public void convertSendAndReceivePointcut() {
        // just a pointcut method
    }

    @Pointcut("execution(* org.springframework.amqp.rabbit.core.RabbitTemplate.receiveAndConvert(..))")
    public void receiveAndConvertPointcut() {
        // just a pointcut method
    }

    @Around("convertSendAndReceivePointcut()")
    public Object throwExceptionOnReplyTimeoutNullResponse(ProceedingJoinPoint joinPoint) throws Throwable {
        long timestamp = System.currentTimeMillis();
        Object returnValue = joinPoint.proceed();

        if (returnValue == null && System.currentTimeMillis() - timestamp >= rabbitTemplateReplyTimeout.toMillis()) {
            log.warn("Could not receive message in time, throwing TimeoutQueueException");
            throw new TimeoutQueueException("Timeout occurred");
        } else {
            return returnValue;
        }
    }

    @Around("receiveAndConvertPointcut()")
    public Object throwExceptionOnReceiveTimeoutNullResponse(ProceedingJoinPoint joinPoint) throws Throwable {
        long timestamp = System.currentTimeMillis();
        Object returnValue = joinPoint.proceed();

        if (returnValue == null && System.currentTimeMillis() - timestamp >= rabbitTemplateReceiveTimeout.toMillis()) {
            log.warn("Could not receive message in time, throwing TimeoutQueueException");
            throw new TimeoutQueueException("Timeout occurred");
        } else {
            return returnValue;
        }
    }

}
