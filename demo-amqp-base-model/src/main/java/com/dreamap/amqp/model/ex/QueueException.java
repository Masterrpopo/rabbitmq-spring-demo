package com.dreamap.amqp.model.ex;

public abstract class QueueException extends RuntimeException {
    protected QueueException(String message) {
        super(message);
    }
}
