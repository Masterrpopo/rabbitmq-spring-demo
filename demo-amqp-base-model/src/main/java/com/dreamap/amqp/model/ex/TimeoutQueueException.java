package com.dreamap.amqp.model.ex;

public class TimeoutQueueException extends QueueException {
    public TimeoutQueueException(String message) {
        super(message);
    }
}
