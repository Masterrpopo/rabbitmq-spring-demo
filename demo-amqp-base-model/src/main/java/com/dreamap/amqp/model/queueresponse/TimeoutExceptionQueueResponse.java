package com.dreamap.amqp.model.queueresponse;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TimeoutExceptionQueueResponse extends ErrorQueueResponse {

    @JsonCreator
    public TimeoutExceptionQueueResponse(@JsonProperty("message") String message) {
        super(message);
    }
}
