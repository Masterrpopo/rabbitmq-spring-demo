package com.dreamap.amqp.model.ex;

public class InternalQueueException extends QueueException {
    public InternalQueueException(String message) {
        super(message);
    }
}
