package com.dreamap.amqp.model.config;

import com.dreamap.amqp.model.queueresponse.InternalExceptionQueueResponse;
import com.dreamap.amqp.model.queueresponse.TimeoutExceptionQueueResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@RequiredArgsConstructor
public class QueueResponseBaseJacksonConfig {
    private final ObjectMapper objectMapper;

    @PostConstruct
    public void registerQueueResponseErrorSubtypes() {
        objectMapper.registerSubtypes(new NamedType(TimeoutExceptionQueueResponse.class, "timeoutExceptionQueueResponse"));
        objectMapper.registerSubtypes(new NamedType(InternalExceptionQueueResponse.class, "internalExceptionQueueResponse"));
    }

}
