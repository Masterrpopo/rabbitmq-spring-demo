package com.dreamap.amqp.model.queueresponse;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
public class InternalExceptionQueueResponse extends ErrorQueueResponse {
    @JsonCreator
    public InternalExceptionQueueResponse(@JsonProperty("message") String message) {
        super(message);
    }
}
