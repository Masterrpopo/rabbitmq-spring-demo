package com.dreamap.amqp.model.queueresponse;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY, property = "type")
public abstract class ErrorQueueResponse {
    @Getter
    private final String message;

    protected ErrorQueueResponse(String message) {
        this.message = message;
    }
}
