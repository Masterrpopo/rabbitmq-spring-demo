package com.dreamap.demorabbitmqspringclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoRabbitMqClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoRabbitMqClientApplication.class, args);
    }

}
