package com.dreamap.demorabbitmqspringclient.service;

import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.commons.config.amqp.ErrorAmqpConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@SuppressWarnings("RedundantThrows")
@Service
public class DemoErrorRabbitClient extends RabbitClient {
    private final ErrorAmqpConfig config;

    public DemoErrorRabbitClient(RabbitTemplate rabbitTemplate, ErrorAmqpConfig errorAmqpConfig) {
        super(rabbitTemplate);
        this.config = errorAmqpConfig;
    }

    public Integer timeout() throws QueueException {
        return rabbitTemplate.convertSendAndReceiveAsType(config.getErrorExchangeName(), config.getTimeoutRouting(), EMPTY_MESSAGE, new ParameterizedTypeReference<>() {});
    }

    public Integer customTimeout() throws QueueException {
        return rabbitTemplate.convertSendAndReceiveAsType(config.getErrorExchangeName(), config.getCustomTimeoutRouting(), EMPTY_MESSAGE, new ParameterizedTypeReference<>() {});
    }

    public Integer illegalState() throws QueueException {
        return rabbitTemplate.convertSendAndReceiveAsType(config.getErrorExchangeName(), config.getIllegalStateRouting(), EMPTY_MESSAGE, new ParameterizedTypeReference<>() {});
    }

    public Integer httpStatus() throws QueueException {
        return rabbitTemplate.convertSendAndReceiveAsType(config.getErrorExchangeName(), config.getHttpStatusRouting(), EMPTY_MESSAGE, new ParameterizedTypeReference<>() {});
    }

    public Integer customException() throws QueueException {
        return rabbitTemplate.convertSendAndReceiveAsType(config.getErrorExchangeName(), config.getCustomExceptionRouting(), EMPTY_MESSAGE, new ParameterizedTypeReference<>() {});
    }

    public Integer customQueueException() throws QueueException {
        return rabbitTemplate.convertSendAndReceiveAsType(config.getErrorExchangeName(), config.getCustomQueueExceptionRouting(), EMPTY_MESSAGE, new ParameterizedTypeReference<>() {});
    }
}
