package com.dreamap.demorabbitmqspringclient.service;

import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.commons.config.amqp.CalculationAmqpConfig;
import com.dreamap.commons.dto.request.CalculateSumRequest;
import com.dreamap.commons.dto.response.PossiblePositivePartsResponse;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@SuppressWarnings("RedundantThrows")
@Service
public class DemoCalculationRabbitClient extends RabbitClient {
    private final CalculationAmqpConfig calculationAmqpConfig;

    public DemoCalculationRabbitClient(RabbitTemplate rabbitTemplate, CalculationAmqpConfig calculationAmqpConfig) {
        super(rabbitTemplate);
        this.calculationAmqpConfig = calculationAmqpConfig;
    }

    public Double calculateSum(Double num1, Double num2) throws QueueException {
        return rabbitTemplate.convertSendAndReceiveAsType(calculationAmqpConfig.getCalculationExchangeName(), calculationAmqpConfig.getSumCalculationQueueName(), new CalculateSumRequest(num1, num2), new ParameterizedTypeReference<>() {});
    }

    public Long calculateFactorial(Long base) throws QueueException {
        return rabbitTemplate.convertSendAndReceiveAsType(calculationAmqpConfig.getCalculationExchangeName(), calculationAmqpConfig.getFactorialCalculationQueueName(), base, new ParameterizedTypeReference<>() {});
    }

    public PossiblePositivePartsResponse possiblePositiveParts(Integer number) throws QueueException {
        return rabbitTemplate.convertSendAndReceiveAsType(calculationAmqpConfig.getCalculationExchangeName(), calculationAmqpConfig.getPossiblePositivePartsQueueName(), number, new ParameterizedTypeReference<>() {});
    }
}
