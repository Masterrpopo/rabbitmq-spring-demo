package com.dreamap.demorabbitmqspringclient.service;

import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.commons.config.amqp.MessageStorageAmqpConfig;
import com.dreamap.commons.dto.response.MessagesResponse;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

@SuppressWarnings("RedundantThrows")
@Service
public class DemoMessageStorageRabbitClient extends RabbitClient {
    private final MessageStorageAmqpConfig messageStorageAmqpConfig;

    public DemoMessageStorageRabbitClient(RabbitTemplate rabbitTemplate, MessageStorageAmqpConfig messageStorageAmqpConfig) {
        super(rabbitTemplate);
        this.messageStorageAmqpConfig = messageStorageAmqpConfig;
    }

    public void storeMessage(String message) throws QueueException {
        rabbitTemplate.convertAndSend(messageStorageAmqpConfig.getMessageStorageExchangeName(), messageStorageAmqpConfig.getStoreMessageQueueName(), message);
    }

    public MessagesResponse retrieveMessages() throws QueueException {
        return rabbitTemplate.convertSendAndReceiveAsType(messageStorageAmqpConfig.getMessageStorageExchangeName(), messageStorageAmqpConfig.getRetrieveMessagesQueueName(), EMPTY_MESSAGE, new ParameterizedTypeReference<>() {});
    }
}
