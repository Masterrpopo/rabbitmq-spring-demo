package com.dreamap.demorabbitmqspringclient.service;

import org.springframework.amqp.rabbit.core.RabbitTemplate;

public abstract class RabbitClient {
    protected static final int EMPTY_MESSAGE = 0;

    protected final RabbitTemplate rabbitTemplate;

    protected RabbitClient(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }
}
