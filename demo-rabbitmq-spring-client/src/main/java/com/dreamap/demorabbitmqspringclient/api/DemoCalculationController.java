package com.dreamap.demorabbitmqspringclient.api;

import com.dreamap.commons.dto.response.PossiblePositivePartsResponse;
import com.dreamap.demorabbitmqspringclient.service.DemoCalculationRabbitClient;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class DemoCalculationController {
    private final DemoCalculationRabbitClient calculationRabbitClient;

    /**
     * Synchronously sends custom class as request and receives basic Double as response
     */
    @PostMapping(path = "/sum", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Double calculateSum(Double num1, Double num2) {
        return calculationRabbitClient.calculateSum(num1, num2);
    }

    /**
     * Synchronously sends basic java class as request and receives basic Long as response
     */
    @PostMapping(path = "/factorial", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Long calculateFactorial(Long base) {
        return calculationRabbitClient.calculateFactorial(base);
    }

    /**
     * Synchronously sends basic java class as request and receives custom PossiblePositivePartsResponse as response
     */
    @PostMapping(path = "/possiblePositiveParts", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PossiblePositivePartsResponse possiblePositiveParts(Integer number) {
        return calculationRabbitClient.possiblePositiveParts(number);
    }
}
