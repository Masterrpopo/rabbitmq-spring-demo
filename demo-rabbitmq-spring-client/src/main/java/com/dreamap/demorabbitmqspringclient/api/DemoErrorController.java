package com.dreamap.demorabbitmqspringclient.api;

import com.dreamap.demorabbitmqspringclient.service.DemoErrorRabbitClient;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/error")
@RequiredArgsConstructor
public class DemoErrorController {
    private final DemoErrorRabbitClient rabbitClient;

    /**
     * Synchronously send "empty" message and handle timeout on rabbitTemplate
     */
    @GetMapping(path = "/timeout")
    public Integer timeout() {
        return rabbitClient.timeout();
    }

    /**
     * Synchronously send "empty" message and handle TimeoutQueueException thrown on server
     */
    @GetMapping(path = "/customTimeout")
    public Integer customTimeout() {
        return rabbitClient.customTimeout();
    }

    /**
     * Synchronously send "empty" message and handle IllegalStateException thrown on server
     */
    @GetMapping(path = "/illegalState")
    public Integer illegalState() {
        return rabbitClient.illegalState();
    }

    /**
     * Synchronously send "empty" message and handle HttpStatusCodeException thrown on server
     */
    @GetMapping(path = "/httpStatus")
    public Integer httpStatus() {
        return rabbitClient.httpStatus();
    }

    /**
     * Synchronously send "empty" message and handle custom exception (not extending QueueException) thrown on server
     */
    @GetMapping(path = "/customException")
    public Integer customException() {
        return rabbitClient.customException();
    }

    /**
     * Synchronously send "empty" message and handle custom exception (extending QueueException) thrown on server
     */
    @GetMapping(path = "/customQueueException")
    public Integer customQueueException() {
        return rabbitClient.customQueueException();
    }
}
