package com.dreamap.demorabbitmqspringclient.api;

import com.dreamap.commons.dto.response.MessagesResponse;
import com.dreamap.demorabbitmqspringclient.service.DemoMessageStorageRabbitClient;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class DemoMessageStoreController {
    private final DemoMessageStorageRabbitClient messageStorageRabbitClient;

    /**
     * Send message without reply
     */
    @PostMapping(path = "/message", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public void storeMessage(String message) {
        messageStorageRabbitClient.storeMessage(message);
    }

    /**
     * Send "empty" request when RabbitListener expects no request body just to get reply
     */
    @GetMapping(path = "/message", produces = MediaType.APPLICATION_JSON_VALUE)
    public MessagesResponse getMessages() {
        return messageStorageRabbitClient.retrieveMessages();
    }
}
