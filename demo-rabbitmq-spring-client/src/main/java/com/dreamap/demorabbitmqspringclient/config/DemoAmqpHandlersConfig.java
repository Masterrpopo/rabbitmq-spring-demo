package com.dreamap.demorabbitmqspringclient.config;

import com.dreamap.rabbitmqerrorhandler.QueueExceptionMessagePostProcessor;
import com.dreamap.rabbitmqerrorhandler.RabbitTemplateTimeoutHandler;
import com.dreamap.rabbitmqerrorhandler.RestControllerQueueExceptionHandler;
import com.dreamap.rabbitmqerrorhandler.factory.ex.QueueExceptionFactory;
import com.dreamap.rabbitmqerrorhandler.factory.rest.QueueExceptionResponseEntityFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.List;

@Configuration
@ComponentScan(basePackages = {"com.dreamap.rabbitmqerrorhandler.factory", "com.dreamap.commons.factory"})
public class DemoAmqpHandlersConfig {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private QueueExceptionMessagePostProcessor queueExceptionMessagePostProcessor;

    @Bean
    public RestControllerQueueExceptionHandler restControllerQueueExceptionHandler(List<QueueExceptionResponseEntityFactory<?>> responseEntityFactories) {
        return new RestControllerQueueExceptionHandler(responseEntityFactories);
    }

    @Bean
    public RabbitTemplateTimeoutHandler rabbitTemplateTimeoutAspect(@Value("${spring.rabbitmq.template.reply-timeout}") Duration timeoutDuration) {
        return new RabbitTemplateTimeoutHandler(timeoutDuration, timeoutDuration);
    }

    @Bean
    public QueueExceptionMessagePostProcessor queueExceptionMessagePostProcessor(List<QueueExceptionFactory> queueExceptionFactories, ObjectMapper objectMapper) {
        return new QueueExceptionMessagePostProcessor(queueExceptionFactories, objectMapper);
    }

    @PostConstruct
    public void configureAfterReceiveQueueExceptionPostProcessor() {
        rabbitTemplate.addAfterReceivePostProcessors(queueExceptionMessagePostProcessor);
    }

}
