package com.dreamap.demorabbitmqspringclient.config;

import com.dreamap.commons.config.amqp.CalculationAmqpConfig;
import com.dreamap.commons.config.amqp.ErrorAmqpConfig;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DemoAmqpConfig {
    @Bean
    public DirectExchange calculationExchange(CalculationAmqpConfig calculationAmqpConfig) {
        return new DirectExchange(calculationAmqpConfig.getCalculationExchangeName());
    }

    @Bean
    public TopicExchange errorExchange(ErrorAmqpConfig errorAmqpConfig) {
        return new TopicExchange(errorAmqpConfig.getErrorExchangeName());
    }
}
