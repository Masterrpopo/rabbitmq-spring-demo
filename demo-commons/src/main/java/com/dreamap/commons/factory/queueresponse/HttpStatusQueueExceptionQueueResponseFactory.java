package com.dreamap.commons.factory.queueresponse;

import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import com.dreamap.commons.dto.queueresponse.HttpStatusQueueExceptionQueueResponse;
import com.dreamap.commons.ex.queue.HttpStatusQueueException;
import com.dreamap.rabbitmqerrorhandler.factory.queueresponse.AbstractErrorQueueResponseFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpStatusCodeException;

@Component
public class HttpStatusQueueExceptionQueueResponseFactory extends AbstractErrorQueueResponseFactory {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return HttpStatusCodeException.class.isAssignableFrom(cls) || HttpStatusQueueException.class.isAssignableFrom(cls);
    }

    @Override
    public ErrorQueueResponse buildErrorQueueResponse(Throwable throwable) {
        if (throwable instanceof HttpStatusCodeException httpStatusCodeException) {
            return new HttpStatusQueueExceptionQueueResponse(httpStatusCodeException.getStatusText(), httpStatusCodeException.getStatusCode());
        } else if (throwable instanceof HttpStatusQueueException httpStatusQueueException) {
            return new HttpStatusQueueExceptionQueueResponse(httpStatusQueueException.getMessage(), httpStatusQueueException.getHttpStatus());
        } else {
            throw buildUnsupportedTypeException(throwable);
        }
    }
}
