package com.dreamap.commons.factory.rest;

import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.amqp.model.ex.TimeoutQueueException;
import com.dreamap.rabbitmqerrorhandler.factory.rest.AbstractQueueExceptionResponseEntityFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class TimeoutQueueExceptionResponseEntityFactory extends AbstractQueueExceptionResponseEntityFactory<String> {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return TimeoutQueueException.class.isAssignableFrom(cls);
    }

    @Override
    public ResponseEntity<String> buildResponseEntity(QueueException input) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Connecting over rabbit queue timed out");
    }

    @Override
    public Class<? extends String> getBodyType() {
        return String.class;
    }

}
