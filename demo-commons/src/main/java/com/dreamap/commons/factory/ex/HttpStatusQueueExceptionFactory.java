package com.dreamap.commons.factory.ex;

import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import com.dreamap.commons.dto.queueresponse.HttpStatusQueueExceptionQueueResponse;
import com.dreamap.commons.ex.queue.HttpStatusQueueException;
import com.dreamap.rabbitmqerrorhandler.factory.ex.AbstractQueueExceptionFactory;
import org.springframework.stereotype.Component;

@Component
public class HttpStatusQueueExceptionFactory extends AbstractQueueExceptionFactory {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return HttpStatusQueueExceptionQueueResponse.class.isAssignableFrom(cls);
    }

    @Override
    public QueueException buildException(ErrorQueueResponse errorQueueResponse) {
        final HttpStatusQueueExceptionQueueResponse httpStatusQueueExceptionQueueResponse = (HttpStatusQueueExceptionQueueResponse) errorQueueResponse;
        return new HttpStatusQueueException(httpStatusQueueExceptionQueueResponse.getMessage(), httpStatusQueueExceptionQueueResponse.getHttpStatus());
    }
}
