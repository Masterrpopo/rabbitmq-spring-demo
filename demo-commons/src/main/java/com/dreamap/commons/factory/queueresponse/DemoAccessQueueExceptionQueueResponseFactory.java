package com.dreamap.commons.factory.queueresponse;

import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import com.dreamap.commons.dto.queueresponse.DemoAccessQueueExceptionQueueResponse;
import com.dreamap.commons.ex.DemoAccessException;
import com.dreamap.commons.ex.queue.DemoAccessQueueException;
import com.dreamap.rabbitmqerrorhandler.factory.queueresponse.AbstractErrorQueueResponseFactory;
import org.springframework.stereotype.Component;

@Component
public class DemoAccessQueueExceptionQueueResponseFactory extends AbstractErrorQueueResponseFactory {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return DemoAccessException.class.isAssignableFrom(cls) || DemoAccessQueueException.class.isAssignableFrom(cls);
    }

    @Override
    public ErrorQueueResponse buildErrorQueueResponse(Throwable throwable) {
        if (throwable instanceof DemoAccessException demoAccessException) {
            return new DemoAccessQueueExceptionQueueResponse(demoAccessException.getMessage(), demoAccessException.getUsername());
        } else if (throwable instanceof DemoAccessQueueException demoAccessQueueException) {
            return new DemoAccessQueueExceptionQueueResponse(demoAccessQueueException.getMessage(), demoAccessQueueException.getUsername());
        } else {
            throw buildUnsupportedTypeException(throwable);
        }
    }
}
