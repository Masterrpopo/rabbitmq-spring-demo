package com.dreamap.commons.factory.rest;

import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.commons.ex.queue.DemoAccessQueueException;
import com.dreamap.rabbitmqerrorhandler.factory.rest.AbstractQueueExceptionResponseEntityFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class DemoAccessQueueExceptionResponseEntityFactory extends AbstractQueueExceptionResponseEntityFactory<String> {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return DemoAccessQueueException.class.isAssignableFrom(cls);
    }

    @Override
    public ResponseEntity<String> buildResponseEntity(QueueException input) {
        final DemoAccessQueueException demoAccessQueueException = (DemoAccessQueueException) input;
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("User `%s` cannot access the resource".formatted(demoAccessQueueException.getUsername()));
    }

    @Override
    public Class<? extends String> getBodyType() {
        return String.class;
    }

}
