package com.dreamap.commons.factory.queueresponse;

import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import com.dreamap.commons.dto.queueresponse.DemoCustomQueueExceptionQueueResponse;
import com.dreamap.commons.ex.queue.DemoCustomQueueException;
import com.dreamap.rabbitmqerrorhandler.factory.queueresponse.AbstractErrorQueueResponseFactory;
import org.springframework.stereotype.Component;

@Component
public class DemoCustomQueueExceptionQueueResponseFactory extends AbstractErrorQueueResponseFactory {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return DemoCustomQueueException.class.isAssignableFrom(cls);
    }

    @Override
    public ErrorQueueResponse buildErrorQueueResponse(Throwable throwable) {
        if (throwable instanceof DemoCustomQueueException demoCustomQueueException) {
            return new DemoCustomQueueExceptionQueueResponse(demoCustomQueueException.getMessage(), demoCustomQueueException.getSomeValue());
        } else {
            throw buildUnsupportedTypeException(throwable);
        }
    }
}
