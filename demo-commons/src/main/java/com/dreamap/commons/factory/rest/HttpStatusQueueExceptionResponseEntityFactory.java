package com.dreamap.commons.factory.rest;

import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.commons.ex.queue.HttpStatusQueueException;
import com.dreamap.rabbitmqerrorhandler.factory.rest.AbstractQueueExceptionResponseEntityFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class HttpStatusQueueExceptionResponseEntityFactory extends AbstractQueueExceptionResponseEntityFactory<String> {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return HttpStatusQueueException.class.isAssignableFrom(cls);
    }

    @Override
    public ResponseEntity<String> buildResponseEntity(QueueException input) {
        final HttpStatusQueueException httpStatusQueueException = (HttpStatusQueueException) input;
        return ResponseEntity.status(httpStatusQueueException.getHttpStatus()).body(httpStatusQueueException.getMessage());
    }

    @Override
    public Class<? extends String> getBodyType() {
        return String.class;
    }

}
