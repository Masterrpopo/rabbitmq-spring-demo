package com.dreamap.commons.factory.rest;

import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.commons.dto.queueresponse.DemoCustomQueueExceptionQueueResponse;
import com.dreamap.commons.ex.queue.DemoCustomQueueException;
import com.dreamap.rabbitmqerrorhandler.factory.rest.AbstractQueueExceptionResponseEntityFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class DemoCustomQueueExceptionResponseEntityFactory extends AbstractQueueExceptionResponseEntityFactory<DemoCustomQueueExceptionQueueResponse> {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return DemoCustomQueueException.class.isAssignableFrom(cls);
    }

    @Override
    public ResponseEntity<DemoCustomQueueExceptionQueueResponse> buildResponseEntity(QueueException input) {
        final DemoCustomQueueException queueException = (DemoCustomQueueException) input;
        final DemoCustomQueueExceptionQueueResponse queueResponse = new DemoCustomQueueExceptionQueueResponse(queueException.getMessage(), queueException.getSomeValue());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(queueResponse);
    }

    @Override
    public Class<DemoCustomQueueExceptionQueueResponse> getBodyType() {
        return DemoCustomQueueExceptionQueueResponse.class;
    }
}
