package com.dreamap.commons.factory.ex;

import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import com.dreamap.commons.dto.queueresponse.DemoCustomQueueExceptionQueueResponse;
import com.dreamap.commons.ex.queue.DemoCustomQueueException;
import com.dreamap.rabbitmqerrorhandler.factory.ex.AbstractQueueExceptionFactory;
import org.springframework.stereotype.Component;

@Component
public class DemoCustomQueueExceptionFactory extends AbstractQueueExceptionFactory {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return DemoCustomQueueExceptionQueueResponse.class.isAssignableFrom(cls);
    }

    @Override
    public QueueException buildException(ErrorQueueResponse errorQueueResponse) {
        final DemoCustomQueueExceptionQueueResponse demoCustomQueueExceptionQueueResponse = (DemoCustomQueueExceptionQueueResponse) errorQueueResponse;
        return new DemoCustomQueueException(demoCustomQueueExceptionQueueResponse.getMessage(), demoCustomQueueExceptionQueueResponse.getSomeValue());
    }
}
