package com.dreamap.commons.factory.ex;

import com.dreamap.amqp.model.ex.QueueException;
import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import com.dreamap.commons.dto.queueresponse.DemoAccessQueueExceptionQueueResponse;
import com.dreamap.commons.ex.queue.DemoAccessQueueException;
import com.dreamap.rabbitmqerrorhandler.factory.ex.AbstractQueueExceptionFactory;
import org.springframework.stereotype.Component;

@Component
public class DemoAccessQueueExceptionFactory extends AbstractQueueExceptionFactory {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return DemoAccessQueueExceptionQueueResponse.class.isAssignableFrom(cls);
    }

    @Override
    public QueueException buildException(ErrorQueueResponse errorQueueResponse) {
        final DemoAccessQueueExceptionQueueResponse demoAccessQueueExceptionQueueResponse = (DemoAccessQueueExceptionQueueResponse) errorQueueResponse;
        return new DemoAccessQueueException(demoAccessQueueExceptionQueueResponse.getMessage(), demoAccessQueueExceptionQueueResponse.getUsername());
    }
}
