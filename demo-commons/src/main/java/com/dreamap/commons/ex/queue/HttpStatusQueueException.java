package com.dreamap.commons.ex.queue;

import com.dreamap.amqp.model.ex.QueueException;
import lombok.Getter;
import org.springframework.http.HttpStatus;

public class HttpStatusQueueException extends QueueException {
    @Getter
    private final HttpStatus httpStatus;

    public HttpStatusQueueException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
