package com.dreamap.commons.ex.queue;

import com.dreamap.amqp.model.ex.QueueException;
import lombok.Getter;

public class DemoCustomQueueException extends QueueException {
    @Getter
    private final String someValue;

    public DemoCustomQueueException(String message, String someValue) {
        super(message);
        this.someValue = someValue;
    }
}
