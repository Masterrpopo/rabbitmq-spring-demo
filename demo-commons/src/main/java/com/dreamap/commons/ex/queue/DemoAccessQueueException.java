package com.dreamap.commons.ex.queue;

import com.dreamap.amqp.model.ex.QueueException;
import lombok.Getter;

public class DemoAccessQueueException extends QueueException {
    @Getter
    private final String username;

    public DemoAccessQueueException(String message, String username) {
        super(message);
        this.username = username;
    }
}
