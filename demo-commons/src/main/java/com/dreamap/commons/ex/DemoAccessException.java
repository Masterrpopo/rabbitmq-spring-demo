package com.dreamap.commons.ex;

import lombok.Getter;

public class DemoAccessException extends Exception {
    @Getter
    private final String username;

    public DemoAccessException(String message, String username) {
        super(message);
        this.username = username;
    }
}
