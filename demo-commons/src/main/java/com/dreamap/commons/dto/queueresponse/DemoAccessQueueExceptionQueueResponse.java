package com.dreamap.commons.dto.queueresponse;

import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode(callSuper = true)
public class DemoAccessQueueExceptionQueueResponse extends ErrorQueueResponse {
    @Getter
    private final String username;

    @JsonCreator
    public DemoAccessQueueExceptionQueueResponse(@JsonProperty("message") String message, @JsonProperty("username") String username) {
        super(message);
        this.username = username;
    }
}
