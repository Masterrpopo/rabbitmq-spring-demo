package com.dreamap.commons.dto.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.List;

public class MessagesResponse {
    @Getter
    private final List<String> messages;

    @JsonCreator
    public MessagesResponse(@JsonProperty("messages") List<String> messages) {
        this.messages = messages;
    }
}
