package com.dreamap.commons.dto.response;

import com.dreamap.commons.dto.PossiblePositiveParts;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.List;

public class PossiblePositivePartsResponse {
    @Getter
    private final List<PossiblePositiveParts> possiblePositivePartsList;

    @JsonCreator
    public PossiblePositivePartsResponse(@JsonProperty("possiblePositivePartsList") List<PossiblePositiveParts> possiblePositivePartsList) {
        this.possiblePositivePartsList = possiblePositivePartsList;
    }
}
