package com.dreamap.commons.dto.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@EqualsAndHashCode
public class CalculateSumRequest {
    @Getter
    private final Double num1;

    @Getter
    private final Double num2;

    @JsonCreator
    public CalculateSumRequest(@JsonProperty("num1") Double num1, @JsonProperty("num2") Double num2) {
        this.num1 = num1;
        this.num2 = num2;
    }
}
