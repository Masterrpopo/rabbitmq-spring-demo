package com.dreamap.commons.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

public class PossiblePositiveParts {
    @Getter
    private final Integer part1;
    @Getter
    private final Integer part2;

    @JsonCreator
    public PossiblePositiveParts(@JsonProperty("part1") Integer part1, @JsonProperty("part2") Integer part2) {
        this.part1 = part1;
        this.part2 = part2;
    }
}
