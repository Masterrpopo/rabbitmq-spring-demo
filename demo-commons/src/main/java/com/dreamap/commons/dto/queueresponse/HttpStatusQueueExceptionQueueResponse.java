package com.dreamap.commons.dto.queueresponse;

import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@EqualsAndHashCode(callSuper = true)
public class HttpStatusQueueExceptionQueueResponse extends ErrorQueueResponse {
    @Getter
    private final HttpStatus httpStatus;

    @JsonCreator
    public HttpStatusQueueExceptionQueueResponse(@JsonProperty("message") String message, @JsonProperty("httpStatus") HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
