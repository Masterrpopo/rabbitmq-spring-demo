package com.dreamap.commons.dto.queueresponse;

import com.dreamap.amqp.model.queueresponse.ErrorQueueResponse;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@ToString
@EqualsAndHashCode(callSuper = true)
public class DemoCustomQueueExceptionQueueResponse extends ErrorQueueResponse {
    @Getter
    private final String someValue;

    @JsonCreator
    public DemoCustomQueueExceptionQueueResponse(@JsonProperty("message") String message, @JsonProperty("someValue") String someValue) {
        super(message);
        this.someValue = someValue;
    }
}
