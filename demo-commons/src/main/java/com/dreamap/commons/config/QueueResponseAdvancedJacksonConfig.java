package com.dreamap.commons.config;

import com.dreamap.commons.dto.queueresponse.DemoAccessQueueExceptionQueueResponse;
import com.dreamap.commons.dto.queueresponse.DemoCustomQueueExceptionQueueResponse;
import com.dreamap.commons.dto.queueresponse.HttpStatusQueueExceptionQueueResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.NamedType;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@RequiredArgsConstructor
public class QueueResponseAdvancedJacksonConfig {
    private final ObjectMapper objectMapper;

    @PostConstruct
    public void registerErrorQueueResponseSubtypes() {
        objectMapper.registerSubtypes(new NamedType(DemoCustomQueueExceptionQueueResponse.class, "demoCustomQueueExceptionQueueResponse"));
        objectMapper.registerSubtypes(new NamedType(DemoAccessQueueExceptionQueueResponse.class, "demoAccessQueueExceptionQueueResponse"));
        objectMapper.registerSubtypes(new NamedType(HttpStatusQueueExceptionQueueResponse.class, "httpStatusQueueExceptionQueueResponse"));
    }

}
