package com.dreamap.commons.config.amqp;

import com.dreamap.commons.config.YamlPropertySourceFactory;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Getter
@Configuration
@PropertySource(value = "classpath:AmqpNames.yml", factory = YamlPropertySourceFactory.class)
public class CalculationAmqpConfig {
    @Value("${exchange.calculation.name}")
    private String calculationExchangeName;

    @Value("${exchange.calculation.queue.sum.name}")
    private String sumCalculationQueueName;

    @Value("${exchange.calculation.queue.factorial.name}")
    private String factorialCalculationQueueName;

    @Value("${exchange.calculation.queue.possiblePositiveParts.name}")
    private String possiblePositivePartsQueueName;
}
