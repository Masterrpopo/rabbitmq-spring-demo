package com.dreamap.commons.config.amqp;

import com.dreamap.commons.config.YamlPropertySourceFactory;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Getter
@Configuration
@PropertySource(value = "classpath:AmqpNames.yml", factory = YamlPropertySourceFactory.class)
public class ErrorAmqpConfig {
    @Value("${exchange.error.name}")
    private String errorExchangeName;

    @Value("${exchange.error.queue.timeout.name}")
    private String timeoutQueueName;

    @Value("${exchange.error.queue.timeout.routing}")
    private String timeoutRouting;

    @Value("${exchange.error.queue.customTimeout.name}")
    private String customTimeoutQueueName;

    @Value("${exchange.error.queue.customTimeout.routing}")
    private String customTimeoutRouting;

    @Value("${exchange.error.queue.illegalState.name}")
    private String illegalStateQueueName;

    @Value("${exchange.error.queue.illegalState.routing}")
    private String illegalStateRouting;

    @Value("${exchange.error.queue.httpStatus.name}")
    private String httpStatusQueueName;

    @Value("${exchange.error.queue.httpStatus.routing}")
    private String httpStatusRouting;

    @Value("${exchange.error.queue.customException.name}")
    private String customExceptionQueueName;

    @Value("${exchange.error.queue.customException.routing}")
    private String customExceptionRouting;

    @Value("${exchange.error.queue.customQueueException.name}")
    private String customQueueExceptionQueueName;

    @Value("${exchange.error.queue.customQueueException.routing}")
    private String customQueueExceptionRouting;
}
