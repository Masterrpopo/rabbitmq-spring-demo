package com.dreamap.commons.config.amqp;

import com.dreamap.commons.config.YamlPropertySourceFactory;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;


@Getter
@Configuration
@PropertySource(value = "classpath:AmqpNames.yml", factory = YamlPropertySourceFactory.class)
public class MessageStorageAmqpConfig {
    @Value("${exchange.messageStorage.name}")
    private String messageStorageExchangeName;

    @Value("${exchange.messageStorage.queue.store.name}")
    private String storeMessageQueueName;

    @Value("${exchange.messageStorage.queue.retrieve.name}")
    private String retrieveMessagesQueueName;
}
