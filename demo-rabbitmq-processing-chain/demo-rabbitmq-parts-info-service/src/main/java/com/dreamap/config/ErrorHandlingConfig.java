package com.dreamap.config;

import com.dreamap.rabbitmqerrorhandler.CustomRabbitListenerErrorHandler;
import com.dreamap.rabbitmqerrorhandler.factory.queueresponse.ErrorQueueResponseFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class ErrorHandlingConfig {

    @Bean
    public CustomRabbitListenerErrorHandler customRabbitListenerErrorHandler(List<ErrorQueueResponseFactory> queueResponseFactories) {
        return new CustomRabbitListenerErrorHandler(queueResponseFactories);
    }

}
