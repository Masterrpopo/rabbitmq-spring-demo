package com.dreamap.service;

import com.dreamap.dto.CarPart;
import com.dreamap.dto.CarPartType;
import com.dreamap.dto.ClutchCarPart;
import com.dreamap.dto.EngineCarPart;
import org.springframework.stereotype.Service;

import java.time.Period;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class PartsManager {

    public List<CarPart> getCarParts(String carBrand, String carModel) {
        if (carBrand.equalsIgnoreCase("volkswagen") && carModel.equalsIgnoreCase("polo")) {
            CarPart wheels = new CarPart();
            wheels.setId("wheels-1");
            wheels.setType(CarPartType.WHEELS);
            wheels.setModel("Big wheels 8000");

            EngineCarPart engine = new EngineCarPart("Quick engine 123", 150d, 2.0);
            engine.setId("engine-1");
            engine.setEstimatedLifespan(Period.of(0, 6, 0));


            ClutchCarPart clutch = new ClutchCarPart("Turbo clutch 3", false, false);
            clutch.setId("clutch-1");
            clutch.setEstimatedLifespan(Period.of(1, 6, 0));

            return Arrays.asList(engine, clutch, wheels);
        } else {
            return Collections.emptyList();
        }
    }
}
