package com.dreamap.controler;

import com.dreamap.CarInfoRabbitClient;
import com.dreamap.dto.CarInfo;
import com.dreamap.dto.CarPart;
import com.dreamap.service.PartsManager;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class PartsLoaderController {
    private final CarInfoRabbitClient carInfoRabbitClient;
    private final PartsManager partsManager;

    @RabbitListener(queues = "${exchange.carInfo.queue.loadParts.name}", errorHandler = "customRabbitListenerErrorHandler")
    public void loadParts(@Payload CarInfo partialCarInfo, Message message) {
        if (partialCarInfo.getCarModel().equals("model-throwExceptionInPartsInfoService")) {
            throw new IllegalStateException("Parts info service illegal state exception");
        } else {
            List<CarPart> carParts = partsManager.getCarParts(partialCarInfo.getCarBrand(), partialCarInfo.getCarModel());
            partialCarInfo.setParts(carParts);

            carInfoRabbitClient.runLoadReplacementsStep(partialCarInfo, message.getMessageProperties().getReplyTo());
        }
    }
}
