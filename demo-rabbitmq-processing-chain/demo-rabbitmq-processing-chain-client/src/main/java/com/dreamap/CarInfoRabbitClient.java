package com.dreamap;

import com.dreamap.config.CarInfoAmqpConfig;
import com.dreamap.dto.CarInfo;
import org.springframework.amqp.core.AnonymousQueue;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.time.Duration;

@Service
public class CarInfoRabbitClient {
    private final RabbitTemplate rabbitTemplate;
    private final RabbitAdmin rabbitAdmin;
    private final CarInfoAmqpConfig carInfoAmqpConfig;

    private final Duration receiveTimeout;

    public CarInfoRabbitClient(RabbitTemplate rabbitTemplate, RabbitAdmin rabbitAdmin, CarInfoAmqpConfig carInfoAmqpConfig, @Value("${spring.rabbitmq.template.receive-timeout}") Duration receiveTimeout) {
        this.rabbitTemplate = rabbitTemplate;
        this.rabbitAdmin = rabbitAdmin;
        this.carInfoAmqpConfig = carInfoAmqpConfig;
        this.receiveTimeout = receiveTimeout;
    }

    public CarInfo loadFullCarInfo(CarInfo partialCarInfo) {
        AnonymousQueue responseQueue = new AnonymousQueue();
        String responseQueueName = rabbitAdmin.declareQueue(responseQueue);

        if (ObjectUtils.isEmpty(responseQueueName)) {
            throw new IllegalStateException("RabbitMQ broker returned empty queue name");
        }

        try {
            runLoadPartsStep(partialCarInfo, responseQueueName);
            return rabbitTemplate.receiveAndConvert(responseQueueName, receiveTimeout.toMillis(), new ParameterizedTypeReference<>() {});
        } finally {
            rabbitAdmin.deleteQueue(responseQueueName);
        }
    }

    public void runLoadPartsStep(CarInfo carInfo, String replyToQueue) {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setReplyTo(replyToQueue);

        Message message = rabbitTemplate.getMessageConverter().toMessage(carInfo, messageProperties);
        rabbitTemplate.send(carInfoAmqpConfig.getCarInfoExchangeName(), carInfoAmqpConfig.getLoadPartsQueueName(), message);
    }

    public void runLoadReplacementsStep(CarInfo carInfo, String replyToQueue) {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setReplyTo(replyToQueue);

        Message message = rabbitTemplate.getMessageConverter().toMessage(carInfo, messageProperties);
        rabbitTemplate.send(carInfoAmqpConfig.getCarInfoExchangeName(), carInfoAmqpConfig.getLoadReplacementsQueueName(), message);
    }

    public void runLoadRemindersStep(CarInfo carInfo, String replyToQueue) {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setReplyTo(replyToQueue);

        Message message = rabbitTemplate.getMessageConverter().toMessage(carInfo, messageProperties);
        rabbitTemplate.send(carInfoAmqpConfig.getCarInfoExchangeName(), carInfoAmqpConfig.getLoadRemindersQueueName(), message);
    }

}
