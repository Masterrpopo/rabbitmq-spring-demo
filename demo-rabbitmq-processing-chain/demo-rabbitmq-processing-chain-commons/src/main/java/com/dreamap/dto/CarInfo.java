package com.dreamap.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class CarInfo {
    private String carBrand;
    private String carModel;
    private Integer productionYear;
    private BigDecimal salonPrice;

    private List<CarPart> parts;
    private List<CarPartReplacement> replacementParts;
    private List<Reminder> reminders;
}
