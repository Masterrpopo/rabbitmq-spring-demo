package com.dreamap.dto;

public enum CarPartType {
    ENGINE, CLUTCH, BREAKS, WHEELS, TIRES
}
