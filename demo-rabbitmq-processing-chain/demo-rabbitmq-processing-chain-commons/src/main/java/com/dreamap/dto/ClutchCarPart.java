package com.dreamap.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class ClutchCarPart extends CarPart {
    private Boolean automaticEngagement;
    private Boolean preventsStalling;

    public ClutchCarPart() {
        setType(CarPartType.CLUTCH);
    }

    public ClutchCarPart(String model, Boolean automaticEngagement, Boolean preventsStalling) {
        this();
        this.automaticEngagement = automaticEngagement;
        this.preventsStalling = preventsStalling;
        setModel(model);
    }
}
