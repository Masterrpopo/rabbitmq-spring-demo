package com.dreamap.dto;

import lombok.Data;

import java.time.Period;

@Data
public class CarPartReplacement {
    private String originalPartId;
    private String model;
    private Period estimatedLifespan;
}
