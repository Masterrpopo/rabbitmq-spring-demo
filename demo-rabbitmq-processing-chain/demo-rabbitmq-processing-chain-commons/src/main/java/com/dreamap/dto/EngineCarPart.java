package com.dreamap.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class EngineCarPart extends CarPart {
    private Double horsePower;
    private Double capacity;

    public EngineCarPart() {
        setType(CarPartType.ENGINE);
    }

    public EngineCarPart(String model, Double horsePower, Double capacity) {
        this();
        this.horsePower = horsePower;
        this.capacity = capacity;
        setModel(model);
    }
}
