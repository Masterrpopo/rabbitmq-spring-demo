package com.dreamap.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Reminder {
    private LocalDateTime reminderDate;
    private String message;
}
