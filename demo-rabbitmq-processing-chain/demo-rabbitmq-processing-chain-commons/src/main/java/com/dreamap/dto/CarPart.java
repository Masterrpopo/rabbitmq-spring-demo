package com.dreamap.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;

import java.time.Period;

@Data
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY, property = "classType")
@JsonSubTypes({
        @JsonSubTypes.Type(value = EngineCarPart.class, name = "engineCarPart"),
        @JsonSubTypes.Type(value = ClutchCarPart.class, name = "clutchCarPart")
})
public class CarPart {
    private String id;

    private CarPartType type;
    private String model;

    private Period estimatedLifespan;
}
