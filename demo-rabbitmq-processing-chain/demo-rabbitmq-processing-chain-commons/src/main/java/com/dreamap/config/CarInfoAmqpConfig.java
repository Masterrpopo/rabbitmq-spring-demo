package com.dreamap.config;

import com.dreamap.YamlPropertySourceFactory;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Getter
@Configuration
@PropertySource(value = "classpath:AmqpNames.yml", factory = YamlPropertySourceFactory.class)
public class CarInfoAmqpConfig {
    @Value("${exchange.carInfo.name}")
    private String carInfoExchangeName;

    @Value("${exchange.carInfo.queue.loadParts.name}")
    private String loadPartsQueueName;

    @Value("${exchange.carInfo.queue.loadReplacements.name}")
    private String loadReplacementsQueueName;

    @Value("${exchange.carInfo.queue.loadReminders.name}")
    private String loadRemindersQueueName;
}
