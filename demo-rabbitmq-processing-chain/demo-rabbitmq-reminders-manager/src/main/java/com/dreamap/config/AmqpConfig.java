package com.dreamap.config;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class AmqpConfig {
    private final CarInfoAmqpConfig carInfoAmqpConfig;

    @Bean
    public DirectExchange carInfoExchange() {
        return new DirectExchange(carInfoAmqpConfig.getCarInfoExchangeName());
    }

    @Bean
    public Queue loadRemindersQueue() {
        return new Queue(carInfoAmqpConfig.getLoadRemindersQueueName());
    }

    @Bean
    public Binding loadRemindersQueueBinding(DirectExchange carInfoExchange, Queue loadRemindersQueue) {
        return BindingBuilder.bind(loadRemindersQueue).to(carInfoExchange).withQueueName();
    }

    @Bean
    public RabbitAdmin rabbitAdmin(RabbitTemplate rabbitTemplate) {
        return new RabbitAdmin(rabbitTemplate);
    }

}
