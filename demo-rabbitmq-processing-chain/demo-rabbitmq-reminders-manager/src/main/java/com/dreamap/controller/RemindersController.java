package com.dreamap.controller;

import com.dreamap.dto.CarInfo;
import com.dreamap.dto.Reminder;
import com.dreamap.service.RemindersManager;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class RemindersController {
    private final RabbitTemplate rabbitTemplate;
    private final RemindersManager remindersManager;

    @RabbitListener(queues = "${exchange.carInfo.queue.loadReminders.name}", errorHandler = "customRabbitListenerErrorHandler")
    public void loadReminders(@Payload CarInfo partialCarInfo, Message message) {
        if (partialCarInfo.getCarModel().equals("model-throwExceptionInRemindersManager")) {
            throw new IllegalStateException("Reminders manager illegal state exception");
        } else {
            List<Reminder> reminders = remindersManager.generateReminders(partialCarInfo);
            partialCarInfo.setReminders(reminders);

            rabbitTemplate.convertAndSend(message.getMessageProperties().getReplyTo(), partialCarInfo);
        }
    }
}
