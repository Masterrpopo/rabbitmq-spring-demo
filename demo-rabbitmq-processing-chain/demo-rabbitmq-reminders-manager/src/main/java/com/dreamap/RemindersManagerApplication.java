package com.dreamap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource(value = "classpath:application.yaml", factory = YamlPropertySourceFactory.class)
public class RemindersManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RemindersManagerApplication.class, args);
    }

}
