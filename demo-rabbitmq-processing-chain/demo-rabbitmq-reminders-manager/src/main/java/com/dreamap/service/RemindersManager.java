package com.dreamap.service;

import com.dreamap.dto.CarInfo;
import com.dreamap.dto.CarPart;
import com.dreamap.dto.Reminder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class RemindersManager {
    private static final String REMINDER_MESSAGE_TEMPLATE = "You should check your %s - %s at official service";

    public List<Reminder> generateReminders(CarInfo partialCarInfo) {
        return Optional.ofNullable(partialCarInfo)
                .map(CarInfo::getParts)
                .stream().flatMap(Collection::stream)
                .map(this::buildPartServiceReminder)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
    }

    public Optional<Reminder> buildPartServiceReminder(CarPart carPart) {
        if (carPart.getEstimatedLifespan() != null && carPart.getModel() != null) {
            LocalDateTime notificationLocalDateTime = LocalDateTime.now().plus(carPart.getEstimatedLifespan()).minus(2, ChronoUnit.WEEKS);

            String formattedMessage = REMINDER_MESSAGE_TEMPLATE.formatted(carPart.getType().toString(), carPart.getModel());

            Reminder reminder = new Reminder();
            reminder.setReminderDate(notificationLocalDateTime);
            reminder.setMessage(formattedMessage);

            return Optional.of(reminder);
        } else {
            return Optional.empty();
        }
    }
}
