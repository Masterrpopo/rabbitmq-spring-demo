package com.dreamap.service;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class CarInfoLoader {
    public Optional<Integer> getProductionYear(String carBrand, String carModel) {
        if (carBrand.equalsIgnoreCase("volkswagen") && carModel.equalsIgnoreCase("polo")) {
            return Optional.of(1975);
        } else {
            return Optional.empty();
        }
    }

    public Optional<BigDecimal> getSalonPrice(String carBrand, String carModel) {
        if (carBrand.equalsIgnoreCase("volkswagen") && carModel.equalsIgnoreCase("polo")) {
            return Optional.of(new BigDecimal(100000));
        } else {
            return Optional.empty();
        }
    }
}
