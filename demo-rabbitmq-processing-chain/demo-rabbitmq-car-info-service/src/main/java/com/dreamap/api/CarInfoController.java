package com.dreamap.api;

import com.dreamap.CarInfoRabbitClient;
import com.dreamap.dto.CarInfo;
import com.dreamap.service.CarInfoLoader;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.Optional;

@Log4j2
@RestController
@RequiredArgsConstructor
public class CarInfoController {
    private final CarInfoLoader carInfoLoader;
    private final CarInfoRabbitClient carInfoRabbitClient;

    @GetMapping(path = "/car/{carBrand}/{carModel}/info")
    public CarInfo getCarInfo(@PathVariable("carBrand") String carBrand, @PathVariable("carModel") String carModel) {
        CarInfo carInfo = new CarInfo();
        carInfo.setCarBrand(carBrand);
        carInfo.setCarModel(carModel);

        Optional<Integer> productionYear = carInfoLoader.getProductionYear(carBrand, carModel);
        productionYear.ifPresent(carInfo::setProductionYear);

        Optional<BigDecimal> salonPrice = carInfoLoader.getSalonPrice(carBrand, carModel);
        salonPrice.ifPresent(carInfo::setSalonPrice);

        CarInfo fullCarInfo = carInfoRabbitClient.loadFullCarInfo(carInfo);

        log.info("Loaded car info for {} - {}", carBrand, carModel);

        return fullCarInfo;
    }
}
