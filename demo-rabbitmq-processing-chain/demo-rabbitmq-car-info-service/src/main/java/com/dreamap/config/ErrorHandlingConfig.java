package com.dreamap.config;

import com.dreamap.rabbitmqerrorhandler.QueueExceptionMessagePostProcessor;
import com.dreamap.rabbitmqerrorhandler.RabbitTemplateTimeoutHandler;
import com.dreamap.rabbitmqerrorhandler.factory.ex.QueueExceptionFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.time.Duration;
import java.util.List;

@Configuration
@ComponentScan("com.dreamap.rabbitmqerrorhandler.factory")
public class ErrorHandlingConfig {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private QueueExceptionMessagePostProcessor queueExceptionMessagePostProcessor;

    @Bean
    public QueueExceptionMessagePostProcessor queueExceptionMessagePostProcessor(List<QueueExceptionFactory> queueExceptionFactories, ObjectMapper objectMapper) {
        return new QueueExceptionMessagePostProcessor(queueExceptionFactories, objectMapper);
    }

    @Bean
    public RabbitTemplateTimeoutHandler rabbitTemplateTimeoutHandler(@Value("${spring.rabbitmq.template.reply-timeout}") Duration rabbitTemplateReplyTimeout, @Value("${spring.rabbitmq.template.receive-timeout}") Duration rabbitTemplateReceiveTimeout) {
        return new RabbitTemplateTimeoutHandler(rabbitTemplateReplyTimeout, rabbitTemplateReceiveTimeout);
    }

    @PostConstruct
    public void configureAfterReceiveQueueExceptionPostProcessor() {
        rabbitTemplate.addAfterReceivePostProcessors(queueExceptionMessagePostProcessor);
    }


}
