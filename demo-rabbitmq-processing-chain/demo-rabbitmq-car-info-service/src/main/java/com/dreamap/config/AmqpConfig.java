package com.dreamap.config;

import com.dreamap.YamlPropertySourceFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@RequiredArgsConstructor
@PropertySource(value = "classpath:AmqpNames.yml", factory = YamlPropertySourceFactory.class)
public class AmqpConfig {
    private final CarInfoAmqpConfig carInfoAmqpConfig;

    @Bean
    public DirectExchange carInfoExchange() {
        return new DirectExchange(carInfoAmqpConfig.getCarInfoExchangeName());
    }

    @Bean
    public RabbitAdmin rabbitAdmin(RabbitTemplate rabbitTemplate) {
        return new RabbitAdmin(rabbitTemplate);
    }
}
