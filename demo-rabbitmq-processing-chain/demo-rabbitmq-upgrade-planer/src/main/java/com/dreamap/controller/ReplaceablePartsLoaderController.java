package com.dreamap.controller;

import com.dreamap.CarInfoRabbitClient;
import com.dreamap.dto.CarInfo;
import com.dreamap.dto.CarPartReplacement;
import com.dreamap.service.ReplaceablePartsManager;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ReplaceablePartsLoaderController {
    private final CarInfoRabbitClient carInfoRabbitClient;
    private final ReplaceablePartsManager replaceablePartsManager;

    @RabbitListener(queues = "${exchange.carInfo.queue.loadReplacements.name}", errorHandler = "customRabbitListenerErrorHandler")
    public void loadReplaceableParts(@Payload CarInfo parialCarInfo, Message message) {
        if (parialCarInfo.getCarModel().equals("model-throwExceptionInUpgradePlanner")) {
            throw new IllegalStateException("Upgrade planner illegal state exception");
        } else {
            List<CarPartReplacement> replaceableParts = replaceablePartsManager.getReplaceableParts(parialCarInfo.getParts());
            parialCarInfo.setReplacementParts(replaceableParts);

            carInfoRabbitClient.runLoadRemindersStep(parialCarInfo, message.getMessageProperties().getReplyTo());
        }
    }
}
