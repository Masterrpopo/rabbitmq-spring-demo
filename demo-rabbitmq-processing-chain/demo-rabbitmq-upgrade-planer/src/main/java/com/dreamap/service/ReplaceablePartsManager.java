package com.dreamap.service;

import com.dreamap.dto.CarPart;
import com.dreamap.dto.CarPartReplacement;
import org.springframework.stereotype.Service;

import java.time.Period;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ReplaceablePartsManager {

    public List<CarPartReplacement> getReplaceableParts(List<CarPart> originalParts) {
        return Optional.ofNullable(originalParts).stream().flatMap(Collection::stream)
                .filter(Objects::nonNull)
                .map(this::getReplaceableParts)
                .flatMap(Collection::stream)
                .toList();
    }

    private List<CarPartReplacement> getReplaceableParts(CarPart carPart) {
        if (carPart.getId().equalsIgnoreCase("engine-1")) {
            CarPartReplacement greenEngine = new CarPartReplacement();
            greenEngine.setOriginalPartId(carPart.getId());
            greenEngine.setModel("Green engine");
            greenEngine.setEstimatedLifespan(Period.ofYears(2));

            CarPartReplacement blueEngine = new CarPartReplacement();
            blueEngine.setOriginalPartId(carPart.getId());
            blueEngine.setModel("Blue engine");
            blueEngine.setEstimatedLifespan(Period.ofYears(5));

            return Arrays.asList(greenEngine, blueEngine);
        } else if (carPart.getId().equalsIgnoreCase("clutch-1")) {
            CarPartReplacement greenClutch = new CarPartReplacement();
            greenClutch.setOriginalPartId(carPart.getId());
            greenClutch.setModel("Green clutch");
            greenClutch.setEstimatedLifespan(Period.ofYears(2));

            return Collections.singletonList(greenClutch);
        } else {
            return Collections.emptyList();
        }
    }
}
