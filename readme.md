# How this works

---

<!-- TOC -->

* [How this works](#how-this-works)
    * [Intro](#intro)
    * [Reminder - how does RabbitMq work](#reminder---how-does-rabbitmq-work)
    * [Configure amqp](#configure-amqp)
        * [Configuration resource file](#configuration-resource-file)
        * [Configuration class](#configuration-class)
        * [Declare AMQP elements](#declare-amqp-elements)
            * [Basic config](#basic-config)
            * [Client side](#client-side)
            * [Server side](#server-side)
            * [Tips](#tips)
    * [Client handling](#client-handling)
        * [Create rabbit client](#create-rabbit-client)
    * [Server handling](#server-handling)
        * [Create rabbit controller](#create-rabbit-controller)
    * [Error handling](#error-handling)
        * [Concept](#concept)
        * [Client side](#client-side)
            * [Handling errors returned via RabbitMQ](#handling-errors-returned-via-rabbitmq)
            * [Handling RabbitMQ timeouts](#handling-rabbitmq-timeouts)
            * [Handling QueueExceptions on RestControllers](#handling-queueexceptions-on-restcontrollers)
            * [QueueExceptionFactory](#queueexceptionfactory)
            * [Distance in factories](#distance-in-factories)
            * [QueueExceptionResponseEntityFactory](#queueexceptionresponseentityfactory)
        * [Server side](#server-side)
            * [Handling exceptions occurring on server side](#handling-exceptions-occurring-on-server-side)
            * [Propagate unhandled QueueExceptions](#propagate-unhandled-queueexceptions)
            * [Serialization of ErrorQueueResponses](#serialization-of-errorqueueresponses)
    * [Module structure](#module-structure)
        * [Library type modules](#library-type-modules)
            * [AMQP Base Model](#amqp-base-model)
            * [Error Handler](#error-handler)
        * [Project implementation type modules](#project-implementation-type-modules)
            * [Common DTO](#common-dto)
            * [Client](#client)
            * [Server](#server)
        * [Usage / extension](#usage--extension)
            * [Add new api method](#add-new-api-method)
            * [Add support for new exception](#add-support-for-new-exception)
        * [Notes](#notes)
    * [Future ideas / todo](#future-ideas--todo)

<!-- TOC -->

## Intro

The goal of this project is to show my recommended approach to handling synchronized request-response pattern using
Spring RabbitMQ framework. I will show you not only how you can apply successful request-response pattern, but what's
more important - how to handle server side exceptions and transfer them over to client in a sophisticated way. Following
this model you'll be able to fairly easy implement your own AMQP "API", get basic error handling out of the box and
extend error handling to your own needs.  
Keep in mind that this tutorial is not meant to teach how RabbitMQ works, what exchanges, queues or binding are, neither
when you should use RabbitMQ. You can learn these from
official [RabbitMQ Tutorials](https://www.rabbitmq.com/getstarted.html). In this one I will cover some more advanced
stuff.

## Reminder - how does RabbitMq work

The basic request-response can be shown on this diagram.
![](/plantuml/out/rabbit-mq-base-request-response-communication.svg)
As you can see client sends a message to rabbit broker with information about message payload, target exchange, routing
key and reply-to header. Then it starts listening on that reply-to queue and awaits a response message.  
In a meantime server listens on a request queue that receives client's message, receives message, processes it and sends
a response message to declared reply-to queue. Actually it sends message to default exchange with routing key matching
reply-to queue name, but I wanted to keep things simple in diagram.

But what happens if something goes wrong?
![](/plantuml/out/rabbit-mq-base-request-response-error-communication.svg)
If some kind of error occurs on the server while it was processing message it will not send the response message and
leave the client hanging, waiting for a message that will never come. Because of that after a certain configured time
the client will just detect timeout waiting for response and in current RabbitMq library version (hopefully changes in a
future) it doesn't throw any exception, just returns a default value like null for object types or `0`, `false` etc. for
primitives like `int`, `boolean`.

## Visual representation

The way that this implementation works can be shown on following diagram.  
It will be explained step by step in the next chapters. If you feel lost at some point - just check the diagram to know
what you're working on.
![](/plantuml/out/rabbit-mq-demo-error-handling.svg)

## Configure amqp

### Configuration resource file

Create a resource file containing all required AMQP element names like `demo-commons/src/main/resources/AmqpNames.yml`

```yaml
exchange:
  calculation:
    name: calculationExchange
    queue:
      sum:
        name: sumCalculationQueue
      factorial:
        name: factorialCalculationQueue
      possiblePositiveParts:
        name: possiblePositivePartsQueue
  messageStorage:
    name: messageStorageExchange
    queue:
      store:
        name: storeMessageQueue
      retrieve:
        name: retrieveMessagesQueue
  error:
    name: errorExchange
    queue:
      timeout:
        name: timeoutQueue
        routing: timeout.default
      customTimeout:
        name: customTimeoutQueue
        routing: timeout.custom
      illegalState:
        name: illegalStateQueue
        routing: illegalState.default
      httpStatus:
        name: httpStatusQueue
        routing: httpStatus.default
      customException:
        name: customExceptionQueue
        routing: customException.custom
      customQueueException:
        name: customQueueExceptionQueue
        routing: customQueueException.custom
```

Assuming you're implementing both server and client sides this file should include all required names for both ends:

- exchange name e.g. `exchange.calculation.name = calculationExchange`
- queue name e.g. `exchange.calculation.queue.sum.name = sumCalculationQueue`
- routing e.g. `exchange.error.queue.httpStatus.routing = httpStatus.default`
    - this one is optional if queue is bound to exchange with queue name

Keep in mind that this file is just a helper to access names, and it doesn't configure queues or exchanges on its own.

### Configuration class

Bind properties to configuration class
e.g. `demo-commons/src/main/java/com/dreamap/commons/config/amqp/CalculationAmqpConfig.java`

```java

@Getter
@Configuration
@PropertySource(value = "classpath:AmqpNames.yml", factory = YamlPropertySourceFactory.class)
public class CalculationAmqpConfig {
    @Value("${exchange.calculation.name}")
    private String calculationExchangeName;

    @Value("${exchange.calculation.queue.sum.name}")
    private String sumCalculationQueueName;

    @Value("${exchange.calculation.queue.factorial.name}")
    private String factorialCalculationQueueName;

    @Value("${exchange.calculation.queue.possiblePositiveParts.name}")
    private String possiblePositivePartsQueueName;
}
```

1. Make sure that required properties file is loaded as properties with `@PropertySource`
   e.g. `@PropertySource(value = "classpath:AmqpNames.yml", factory = YamlPropertySourceFactory.class`.
2. If you use `.yaml` or `.yml` files with properties you have to implicitly point a `factory` parameter to our
   custom `YamlPropertySourceFactory`.
3. Bind properties with `@Value` e.g. `@Value("${exchange.calculation.name}")`.
4. Make sure that properties are accessible for beans using this configuration class with `@Getter` on either entire
   class or exposed properties.

### Declare AMQP elements

#### Basic config

This part is shared for both client and server side. We need to declare `MessageConverter` and `ObjectMapper` that will
be used to serialize/deserialize messages.

```java

@Configuration
@ComponentScan(basePackages = {"com.dreamap.commons.config", "com.dreamap.amqp.model.config"})
public class DemoBasicConfig {

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public MessageConverter rabbitMessageConverter(ObjectMapper objectMapper) {
        return new Jackson2JsonMessageConverter(objectMapper);
    }

}
```

1. We will use Jackson's `ObjectMapper` to serialize / deserialize java POJOs into serialized Strings used as message
   body.
2. We also need `MessageConverter` to serialize/deserialize whole messages, including theirs headers and properties.
3. We use `@ComponentScan` with base packages `com.dreamap.commons.config` and `com.dreamap.amqp.model.config` to scan
   AMQP configuration beans.
    1. `com.dreamap.amqp.model.config` package contains `QueueResponseBaseJacksonConfig` which contains our custom
       message subtypes linking, so jackson can automatically serialize and deserialize to proper classes (
       see `QueueResponseBaseJacksonConfig` below).
    2. `com.dreamap.commons.config` package contains `YamlPropertySourceFactory`, `QueueResponseAdvancedJacksonConfig`
       and all common property-binding classes like `CalculationAmqpConfig`

```java

@Configuration
@RequiredArgsConstructor
public class QueueResponseBaseJacksonConfig {
    private final ObjectMapper objectMapper;

    @PostConstruct
    public void registerQueueResponseErrorSubtypes() {
        objectMapper.registerSubtypes(new NamedType(TimeoutExceptionQueueResponse.class, "timeoutExceptionQueueResponse"));
        objectMapper.registerSubtypes(new NamedType(InternalExceptionQueueResponse.class, "internalExceptionQueueResponse"));
    }

}
```

#### Client side

Client only needs to declare exchanges - to assure they exist - and have access to routing key (or queue name if queue
is bound to exchange with queue name as routing key). You need to declare these AMQP elements
e.g. `demo-rabbitmq-spring-client/src/main/java/com/dreamap/demorabbitmqspringclient/config/DemoAmqpConfig.java`

```java

@Configuration
public class DemoAmqpConfig {
    @Bean
    public DirectExchange calculationExchange(CalculationAmqpConfig calculationAmqpConfig) {
        return new DirectExchange(calculationAmqpConfig.getCalculationExchangeName());
    }
}
```

For client side you just need to make sure that AMQP `Exchange` is declared e.g. `calculationExchange`. Creating a bean
of any AMQP `Exchange` subtype makes spring boot declare this exchange on application startup via `RabbitAdmin`.

#### Server side

Server on the other hand has to declare all required AMQP elements
e.g. `demo-rabbitmq-spring-server/src/main/java/com/dreamap/demorabbitmqspringserver/config/DemoCalculationAmqpConfig.java`

```java

@Configuration
@RequiredArgsConstructor
public class DemoCalculationAmqpConfig {
    private final CalculationAmqpConfig config;

    @Bean
    public DirectExchange calculationExchange() {
        return new DirectExchange(config.getCalculationExchangeName());
    }

    @Bean
    public Queue sumCalculationQueue() {
        return new Queue(config.getSumCalculationQueueName());
    }

    @Bean
    public Binding sumCalculationBinding(DirectExchange calculationExchange, Queue sumCalculationQueue) {
        return BindingBuilder.bind(sumCalculationQueue).to(calculationExchange).withQueueName();
    }

    @Bean
    public Queue factorialCalculationQueue() {
        return new Queue(config.getFactorialCalculationQueueName());
    }

    @Bean
    public Binding factorialCalculationBinding(DirectExchange calculationExchange, Queue factorialCalculationQueue) {
        return BindingBuilder.bind(factorialCalculationQueue).to(calculationExchange).withQueueName();
    }

    @Bean
    public Queue possiblePositivePartsQueue() {
        return new Queue(config.getPossiblePositivePartsQueueName());
    }

    @Bean
    public Binding possiblePositivePartsBinding(DirectExchange calculationExchange, Queue possiblePositivePartsQueue) {
        return BindingBuilder.bind(possiblePositivePartsQueue).to(calculationExchange).withQueueName();
    }

}
```

1. Declare exchange e.g. `calculationExchange`
2. Declare queue e.g. `sumCalculationQueue`
3. Create binding between exchange and queue e.g. `sumCalculationBinding`
    1. Depending on exchange type you can bind to queue a little different. On `DirectExchange` you bind queue to
       exchange with simple routing key. For this exchange type you can simply bind with queue name or with fixed
       routing key which is recommended in case your queue is `AnonymousQueue` or you need a better control over message
       routing

#### Tips

You can get AMQP names from properties by

1. Constructor injection by declaring final field e.g. `private final CalculationAmqpConfig config`combined with
   lombok's `@RequiredArgsConstructor`
2. Method injection e.g. `public DirectExchange calculationExchange(CalculationAmqpConfig config)`
3. Injecting with `@Value`
   e.g. `public DirectExchange calculationExchange(@Value("${exchange.calculation.name}") String calculationExchangeName)`

## Client handling

### Create rabbit client

To keep things simple and in sync (especially for larger projects) I recommend creating single services
like `DemoCalculationRabbitClient` to easily keep track of usages with IDE and keep communication in sync and
up-to-date.

```java

@Service
public class DemoCalculationRabbitClient extends RabbitClient {
    private final CalculationAmqpConfig calculationAmqpConfig;

    public DemoCalculationRabbitClient(RabbitTemplate rabbitTemplate, CalculationAmqpConfig calculationAmqpConfig) {
        super(rabbitTemplate);
        this.calculationAmqpConfig = calculationAmqpConfig;
    }

    public Double calculateSum(Double num1, Double num2) throws QueueException {
        return rabbitTemplate.convertSendAndReceiveAsType(calculationAmqpConfig.getCalculationExchangeName(), calculationAmqpConfig.getSumCalculationQueueName(), new CalculateSumRequest(num1, num2), new ParameterizedTypeReference<>() {});
    }

    public Long calculateFactorial(Long base) throws QueueException {
        return rabbitTemplate.convertSendAndReceiveAsType(calculationAmqpConfig.getCalculationExchangeName(), calculationAmqpConfig.getFactorialCalculationQueueName(), base, new ParameterizedTypeReference<>() {});
    }

    public PossiblePositivePartsResponse possiblePositiveParts(Integer number) throws QueueException {
        return rabbitTemplate.convertSendAndReceiveAsType(calculationAmqpConfig.getCalculationExchangeName(), calculationAmqpConfig.getPossiblePositivePartsQueueName(), number, new ParameterizedTypeReference<>() {});
    }
}
```

1. Approach that is shown in this project is based on `RabbitTemplate.convertSendAndReceiveAsType(..)` method.
2. When using `RabbitTemplate.convertSendAndReceiveAsType(..)` you have to specify where should the message be sent to.
   That is done by using exchange name and routing key. You can easily obtain these values by simply injecting your
   previously created configuration `CalculationAmqpConfig` and getting values from there
   e.g. `calculationAmqpConfig.getCalculationSumQueueName()`.

## Server handling

### Create rabbit controller

If you want to publish some kind of API over RabbitMQ instead of REST I recommend treating rabbit listeners in a similar
fashion like REST controllers. Create a class per subject to keep your classes small and implement controller logic
inside them, like parsing request, building response, delegating logic to proper services or combining logic from
multiple services.

```java

@Component
@RequiredArgsConstructor
public class DemoCalculationAmqpController {
    private final Calculator calculator;

    @RabbitListener(queues = "${exchange.calculation.queue.sum.name}", errorHandler = "customRabbitListenerErrorHandler")
    public Double sumCalculation(CalculateSumRequest request) {
        return calculator.sum(request.getNum1(), request.getNum2());
    }

    @RabbitListener(queues = "${exchange.calculation.queue.factorial.name}", errorHandler = "customRabbitListenerErrorHandler")
    public Long factorialCalculation(Integer base) {
        return calculator.factorial(base);
    }

    @RabbitListener(queues = "${exchange.calculation.queue.possiblePositiveParts.name}", errorHandler = "customRabbitListenerErrorHandler")
    public PossiblePositivePartsResponse possiblePositiveParts(Integer number) {
        List<PossiblePositiveParts> possiblePositivePartsList = calculator.possiblePositiveParts(number);
        return new PossiblePositivePartsResponse(possiblePositivePartsList);
    }
}
```

1. To declare a method to be a rabbit listener you simply annotate it with `@RabbitListener` and point which queues it
   will be listening to by specifying `queues` parameter. In annotation parameter you cannot use previously declared
   amqp config, but you can use spring value injection similar to how we did in configuration classes previously
   e.g. `${exchange.calculation.queue.sum.name}`.
2. When you declare such a method to return some value Spring RabbitMQ will automatically handle sending the response,
   by using message's `reply-to` field and converting method returned POJO to message using `MessageConverter` we
   registered earlier.
3. To enable custom exception handling (which will be explained later) you need to specify `errorHandler` parameter and
   point to our `customRabbitListenerErrorHandler` (bean name).
4. If you want your listener method to just consume message and don't send anything back - just declare return type to
   be void. This opens up possibilities for processing chains as you can just manually send response to certain exchange
   by using `RabbitTemplate` or your custom `RabbitClient`. Keep in mind you can use basic `Message` as input parameter
   to your `@RabbitListener` method to get access to all parameters like `reply-to` value.

## Error handling

### Concept

First let us understand the concept of error handling.

1. Server receives message and begins handling it
2. While handling message some exception occurs on server
3. Server handles the exception and instead of sending back expected response type it maps the exception to
   some `ErrorQueueResponse` and sends that back instead
4. Client side message post-processor checks if received message can be deserialized to `ErrorQueueResponse` and if that
   happens it maps it to the corresponding QueueException and throws exception - which
   makes `RabbitTemplate.convertSendAndReceiveAsType` throw `QueueException` instead of returning response

### Client side

#### Handling errors returned via RabbitMQ

As mentioned in the concept in case some exception occurs in server side we expect to receive return message with
serialized `ErrorQueueResponse` instead of what we asked for when we used `RabbitTemplate.convertSendAndReceiveAsType()`
. So we set up a message post-processor `QueueExceptionMessagePostProcessor` that will check if response type can be
deserialized to `ErrorQueueResponse` and if that happens it will stop further processing by mapping the
given `ErrorQueueResponse` to corresponding `QueueException` and throwing it - details explained later on.  
Having such message post-processor makes the call to `RabbitTemplate.convertSendAndReceiveAsType()` throw our exception
instead of returning ErrorQueueResponse the server sent us or getting RabbitMQ deserialization exception (
when `ObjectMapper` cannot deserialize message to selected return type passed to `convertSendAndReceiveAsType()` method
e.g. we asked for `Long.class` but received `TimeoutExceptionQueueResponse`).  
We have to keep in mind that just creating a bean of `MessagePostProcessor` type is not enough on its own - we have to
set that as message post-processor for our `RabbitTemplate` explicitly.

```java

@Configuration
public class DemoAmqpHandlersConfig {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private QueueExceptionMessagePostProcessor queueExceptionMessagePostProcessor;

    @Bean
    public QueueExceptionMessagePostProcessor queueExceptionMessagePostProcessor(List<QueueExceptionFactory> queueExceptionFactories, ObjectMapper objectMapper) {
        return new QueueExceptionMessagePostProcessor(queueExceptionFactories, objectMapper);
    }

    @PostConstruct
    public void configureAfterReceiveQueueExceptionPostProcessor() {
        rabbitTemplate.addAfterReceivePostProcessors(queueExceptionMessagePostProcessor);
    }
}
```

#### Handling RabbitMQ timeouts

There is also one extra problem that we have to solve ourselves - at the time of writing this
guide `spring-boot-starter-amqp` doesn't let us know about timeouts occurring
on `RabbitTemplate.convertSendAndReceiveAsType` - instead of throwing an exception it just simply returns null response.
To handle that we have to create our custom AOP advice around that method - `RabbitTemplateTimeoutHandler`.

```java

@Log4j2
@Aspect
@RequiredArgsConstructor
public class RabbitTemplateTimeoutHandler {
    private final Duration rabbitTemplateReplyTimeout;

    @Pointcut("execution(* org.springframework.amqp.rabbit.core.RabbitTemplate.convertSendAndReceiveAsType(..))")
    public void convertSendAndReceivePointcut() {
    }

    @Around("convertSendAndReceivePointcut()")
    public Object throwExceptionOnTimeoutNullResponse(ProceedingJoinPoint joinPoint) throws Throwable {
        long timestamp = System.currentTimeMillis();
        Object returnValue = joinPoint.proceed();

        if (returnValue == null && System.currentTimeMillis() - timestamp >= rabbitTemplateReplyTimeout.toMillis()) {
            log.warn("Could not receive message in time, throwing TimeoutQueueException");
            throw new TimeoutQueueException("Timeout occurred");
        } else {
            return returnValue;
        }
    }
}
```

This handler changes `RabbitTemplate.convertSendAndReceive()` behaviour - if the returned response is null and set
timeout
period has passed.  
We didn't make it a bean by default (no `@Component` or similar annotation on class) so to make it work we still have to
register the bean in spring context with `@Bean`. We did this on purpose, so each module using this aspect can set their
expected timeout duration via constructor parameter.

```java

@Configuration
public class DemoAmqpHandlersConfig {
    @Bean
    public RabbitTemplateTimeoutHandler rabbitTemplateTimeoutAspect(@Value("${spring.rabbitmq.template.reply-timeout}") Duration timeoutDuration) {
        return new RabbitTemplateTimeoutHandler(timeoutDuration);
    }
}
```

As you can see we set the reply timeout for this aspect by `spring.rabbitmq.template.reply-timeout` property.

#### Handling QueueExceptions on RestControllers

To handle the uncaught `QueueException` on our rest controllers we need to provide the bean with `@ControllerAdvice` -
here comes `RestControllerQueueExceptionHandler`.

```java

@Log4j2
@ControllerAdvice
@AllArgsConstructor
public class RestControllerQueueExceptionHandler {
    private final List<QueueExceptionResponseEntityFactory<?>> responseEntityFactories;

    @ExceptionHandler(QueueException.class)
    public ResponseEntity<?> handleException(QueueException queueException) {
        final QueueExceptionResponseEntityFactory<?> queueResponseFactory = getMatchingFactory(queueException);
        final ResponseEntity<?> errorQueueResponseResponseEntity = queueResponseFactory.buildResponseEntity(queueException);

        log.error("Catching %s and building ResponseEntity<%s>".formatted(queueException.getClass().getSimpleName(), queueResponseFactory.getBodyType().getSimpleName()), queueException);
        return errorQueueResponseResponseEntity;
    }

    private QueueExceptionResponseEntityFactory<?> getMatchingFactory(QueueException queueException) {
        return responseEntityFactories.stream()
                .filter(factory -> factory.supportsType(queueException))
                .min(Comparator.comparingInt(factory -> factory.getDistance(queueException)))
                .orElseThrow(() -> new IllegalStateException("Could not find matching QueueExceptionResponseEntityFactory"));
    }
}
```

Let's not focus on `QueueExceptionResponseEntityFactory` for now - this mechanism will be explained in a moment.
Instead, let's focus on the bigger picture. The goal of `@ControllerAdvice` is to be global exception handler for REST
controllers. In case that some exception occurs in REST controller spring checks for handler like this and looks for a
method that can handle given exception - method annotated with `@ExceptionHandler()` with given exception or its
superclass. If it finds one it will call that method and return its response instead.  
In our example we can see that if some `QueueException` occurs in our rest controller and is not caught by controller -
spring will instead call our `public ResponseEntity<?> handleException(QueueException queueException)` method. The
mechanism used here looks for the proper factory that can build `ResponseEntity` out of given `QueueException` and
returns that response.  
Let's not forget that we have to register this as bean in spring context as well.

```java

@Configuration
public class DemoAmqpHandlersConfig {
    @Bean
    public RestControllerQueueExceptionHandler restControllerQueueExceptionHandler(List<QueueExceptionResponseEntityFactory<?>> responseEntityFactories) {
        return new RestControllerQueueExceptionHandler(responseEntityFactories);
    }
}
```

#### QueueExceptionFactory

Back in the `QueueExceptionMessagePostProcessor` - we mention that mechanism of building exceptions out
of `ErrorQueueResponse` will be explained later - and this is the time.  
First let's analyze this `QueueExceptionFactory` interface and `QueueExceptionMessagePostProcessor` logic around it

```java
public interface QueueExceptionFactory {
    boolean supportsType(ErrorQueueResponse errorQueueResponse);

    /**
     * @return Amount of subclasses that occurred between supported and given types
     */
    int getDistance(ErrorQueueResponse errorQueueResponse);

    QueueException buildException(ErrorQueueResponse errorQueueResponse);
}
```

```java

@Log4j2
@RequiredArgsConstructor
public class QueueExceptionMessagePostProcessor implements MessagePostProcessor {
    private final List<QueueExceptionFactory> queueExceptionFactories;
    private final ObjectMapper objectMapper;

    @Override
    public Message postProcessMessage(Message message) throws AmqpException {
        final Optional<ErrorQueueResponse> errorQueueResponse = deserializeToErrorQueueResponse(message.getBody());

        if (errorQueueResponse.isPresent()) {
            throw buildException(errorQueueResponse.get());
        } else {
            return message;
        }
    }

    private Optional<ErrorQueueResponse> deserializeToErrorQueueResponse(byte[] rawMessage) {
        try {
            return Optional.ofNullable(objectMapper.readValue(rawMessage, ErrorQueueResponse.class));
        } catch (IOException ex) {
            return Optional.empty();
        }
    }

    private QueueException buildException(ErrorQueueResponse errorQueueResponse) {
        final QueueExceptionFactory exceptionFactory = getMatchingFactory(errorQueueResponse);
        final QueueException queueException = exceptionFactory.buildException(errorQueueResponse);

        log.error("Received {}, mapping and throwing {}", errorQueueResponse.getClass().getSimpleName(), queueException.getClass().getSimpleName());
        return queueException;
    }

    private QueueExceptionFactory getMatchingFactory(ErrorQueueResponse errorResponse) {
        return queueExceptionFactories.stream()
                .filter(factory -> factory.supportsType(errorResponse))
                .min(Comparator.comparingInt(factory -> factory.getDistance(errorResponse)))
                .orElseThrow(() -> new IllegalStateException("Could not find matching QueueExceptionFactory"));
    }
}
```

As we can see the message post-processor first tries to deserialize the received message into `ErrorQueueResponse` and
if it succeeds then it translates this error response into some `QueueException`.  
Out message post processor gets injected with list of all queue exception factories registered within our spring
context (`private final List<QueueExceptionFactory> queueExceptionFactories` with `@RequiredArgsConstructor`). Then
using streams it filters them for the best factory for exception it has to handle. First we filter out the factories
that do not support our exception, and then we select the one with the closest distance (distance explained in a moment)
. Then it uses selected factory to build ResponseEntity out of handled exception and spring returns it as a
HttpResponse.  
To prevent the situation where we don't have a handler for some `QueueException` we have `FallbackQueueExceptionFactory`
which supports all QueueExceptions, but has the furthest distance (supports top exception type in hierarchy), so it's
only picked if no better factory is found.

#### Distance in factories

The distance property used in our factories is amount of subclasses occurring between concrete object type and factory
supported type. It is better explained on an example. Imagine we have 3 classes

```java
public class FirstClass {}

public class SecondClass extends FirstClass {}

public class ThirdClass extends SecondClass {}
```

And some handlers

```java
public class FirstClassHandler {} // supports FirstClass

public class ThirdClassHandler {} // supports ThirdClass
```

If we look for handlers that support out `ThirdClass` object we get 2 left - how do we determine which one is better to
use? By inheritance distance! We can check how many times we can call `object.getClass().getSuperclass()` until our
factory no longer supports it.  
So in our example if we check distance for `ThirdClass` in `ThirdClassHandler` context - the distance will be zero,
because if we call `thirdClassObject.getClass().getSuperclass()` we get `SecondClass` which is no longer supported by
our `ThirdClassHandler` - therefore the distance (amount of times we can call superclass and still have it supported) is
zero.  
If we check the same for `ThirdClass` in `FirstClassHandler` context we will see that calling superclass for the first
time gives us `SecondClass` - which is still supported (as it's still subtype of supported `FirstClass`), calling
superclass second time gives us `FirstClass` (still supported) and calling it for third time gives us `Object` which is
no longer supported. So the distance for `ThirdClass` in `FirstClassHandler` context is 2.  
This means that `ThirdClassHandler` is more specific and therefore more dedicated for handling `ThirdClass` (closer
distance).

#### QueueExceptionResponseEntityFactory

In a similar fashion to `QueueExceptionFactory` the `QueueExceptionResponseEntityFactory` implementations are used
by `RestControllerQueueExceptionHandler` mentioned earlier - the main difference being that we don't
build `QueueException` out of `ErrorQueueResponse`, but this time we build `ResponseEntity` out of `QueueException`.

```java
/**
 * @param <T> ResponseEntity's body type
 */
public interface QueueExceptionResponseEntityFactory<T> {
    boolean supportsType(QueueException queueException);

    /**
     * @return Amount of subclasses that occurred between supported and given types
     */
    int getDistance(QueueException queueException);

    ResponseEntity<T> buildResponseEntity(QueueException queueException);

    Class<? extends T> getBodyType();
}
```

```java

@Component
public class DemoCustomQueueExceptionResponseEntityFactory extends AbstractQueueExceptionResponseEntityFactory<DemoCustomQueueExceptionQueueResponse> {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return DemoCustomQueueException.class.isAssignableFrom(cls);
    }

    @Override
    public ResponseEntity<DemoCustomQueueExceptionQueueResponse> buildResponseEntity(QueueException input) {
        final DemoCustomQueueException queueException = (DemoCustomQueueException) input;
        final DemoCustomQueueExceptionQueueResponse queueResponse = new DemoCustomQueueExceptionQueueResponse(queueException.getMessage(), queueException.getSomeValue());
        return ResponseEntity.status(HttpStatus.CONFLICT).body(queueResponse);
    }

    @Override
    public Class<DemoCustomQueueExceptionQueueResponse> getBodyType() {
        return DemoCustomQueueExceptionQueueResponse.class;
    }
}
```

To add support for certain exceptions just create an implementation that handles that exception and translates it
into `ResponseEntity` like `DemoCustomQueueExceptionResponseEntityFactory` for example. Remember that in order for it to
actually work it has to be present in spring context, which means either component scanned with some annotation
like `@Component` or registered as a bean in configuration class.
Keep in mind that you don't have to return `ResponseEntity<ErrorQueueResponse>`, you are free to return anything as
response body.

### Server side

#### Handling exceptions occurring on server side

As mentioned in `Error handling / Concept` we have to create a custom `RabbitListenerErrorHandler` to handle uncaught
exceptions that occurred inside our `@RabbitListener` methods.

```java

@Log4j2
@RequiredArgsConstructor
public class CustomRabbitListenerErrorHandler implements RabbitListenerErrorHandler {
    private final List<ErrorQueueResponseFactory> queueResponseFactories;

    @Override
    public Object handleError(Message amqpMessage, org.springframework.messaging.Message<?> message, ListenerExecutionFailedException exception) {
        final Throwable errorCause = exception.getCause();
        final ErrorQueueResponseFactory errorQueueResponseFactory = getMatchingFactory(errorCause);
        final ErrorQueueResponse queueResponse = errorQueueResponseFactory.buildErrorQueueResponse(errorCause);

        log.error("Catching %s and mapping into %s".formatted(errorCause.getClass().getSimpleName(), queueResponse.getClass().getSimpleName()), errorCause);
        return queueResponse;
    }

    private ErrorQueueResponseFactory getMatchingFactory(Throwable throwable) {
        return queueResponseFactories.stream()
                .filter(factory -> factory.supportsType(throwable))
                .min(Comparator.comparingInt(factory -> factory.getDistance(throwable)))
                .orElseThrow(() -> new IllegalStateException("Could not find matching ErrorQueueResponseFactory"));
    }
}
```

First of all when we implement this interface we get our exception wrapped inside `ListenerExecutionFailedException` so
to access the actual exception that was thrown we have to call `exception.getCause()`. After that we handle the
exception (or throwable to be more exact) in same way as we already did in client error handling part.  
We inject our custom error handler with all implementations of `ErrorQueueResponseFactory` and look up the best one to
use. We do so by checking the distance the same way we did earlier to choose most dedicated factory for our given
throwable.

At this moment the `ErrorQueueResponseFactory` interface should not be anything unknown for you

```java
public interface ErrorQueueResponseFactory {
    boolean supportsType(Throwable throwable);

    /**
     * @return Amount of subclasses that occurred between supported and given types
     */
    int getDistance(Throwable throwable);

    ErrorQueueResponse buildErrorQueueResponse(Throwable throwable);
}
```

In order for this error handler to work and get injected with spring beans we need to have it registered as spring bean
either by component scanning and using some annotation like `@Component` or registering it as a bean in our
configuration class.

```java

@Configuration
@ComponentScan(basePackages = {"com.dreamap.rabbitmqerrorhandler.factory", "com.dreamap.commons.factory"})
public class DemoAmqpHandlersConfig {

    @Bean
    public RabbitListenerErrorHandler customRabbitListenerErrorHandler(List<ErrorQueueResponseFactory> queueResponseFactories) {
        return new CustomRabbitListenerErrorHandler(queueResponseFactories);
    }

}
```

This error handler won't be used by default even when registered as bean, so we have to refer to this error handler on
each `@RabbitListener` that we want to use it for by its bean name
e.g. `@RabbitListener(queues = "${exchange.calculation.queue.sum.name}", errorHandler = "customRabbitListenerErrorHandler")`

#### Propagate unhandled QueueExceptions

As a server side application we have to worry about some exception like `DemoAccessException` being thrown inside
our `@RabbitListener` method, but also about getting exception from external services. This server can also be a client
to other services and when we call some service we may also get returned with `ErrorQueueResponse` which will be handled
by our message post processor and translated into some `QueueException`. It means that we have to either catch these and
perform some actions or propagate them.  
If we are to propagate them we need to have a proper `ErrorQueueResponseFactory` that will handle for
example `DemoAccessQueueException` and build some `ErrorQueueResponse` out of it to be sent back to our caller. For
instance, we have `DemoAccessQueueExceptionQueueResponseFactory` which handles both `DemoAccessException`
and `DemoAccessQueueExpception` (2 separate exceptions for demo purposes, one thrown directly in our server, the other
one being queue exception received from our rabbit client).

```java

@Component
public class DemoAccessQueueExceptionQueueResponseFactory extends AbstractErrorQueueResponseFactory {

    @Override
    protected boolean supportsType(Class<?> cls) {
        return DemoAccessException.class.isAssignableFrom(cls) || DemoAccessQueueException.class.isAssignableFrom(cls);
    }

    @Override
    public ErrorQueueResponse buildErrorQueueResponse(Throwable throwable) {
        if (throwable instanceof DemoAccessException demoAccessException) {
            return new DemoAccessQueueExceptionQueueResponse(demoAccessException.getMessage(), demoAccessException.getUsername());
        } else if (throwable instanceof DemoAccessQueueException demoAccessQueueException) {
            return new DemoAccessQueueExceptionQueueResponse(demoAccessQueueException.getMessage(), demoAccessQueueException.getUsername());
        } else {
            throw buildUnsupportedTypeException(throwable);
        }
    }
}
```

In this example we treat both exception the same way, but you can have 2 separate `ErrorQueueResponseFactory`-ies to
handle each one differently. Just remember that you most of the time need to handle these queue exception propagations
if they're not caught. Without such `DemoAccessQueueExpceionQueueResponseFactory` this `DemoAccessQueueException` would
be handled by `FallbackQueueResponseFactory` and just sent back as `InternalExceptionQueueResponse`

#### Serialization of ErrorQueueResponses

In order for our `ObjectMapper` to be able to recognize what kind of `ErrorQueueResponse` it is handling and to
deserialize it to proper class we need to register our custom implementations within object mapper. To avoid having to
extend `JsonSubtypes` annotation on core `ErrorQueueResponse` we can register subtypes directly on `ObjectMapper` after
it's initialization in some configuration class.

```java

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXTERNAL_PROPERTY, property = "type")
public abstract class ErrorQueueResponse {
    @Getter
    private final String message;

    protected ErrorQueueResponse(String message) {
        this.message = message;
    }
}
```

```java

@Configuration
@RequiredArgsConstructor
public class QueueResponseAdvancedJacksonConfig {
    private final ObjectMapper objectMapper;

    @PostConstruct
    public void registerErrorQueueResponseSubtypes() {
        objectMapper.registerSubtypes(new NamedType(DemoCustomQueueExceptionQueueResponse.class, "demoCustomQueueExceptionQueueResponse"));
        objectMapper.registerSubtypes(new NamedType(DemoAccessQueueExceptionQueueResponse.class, "demoAccessQueueExceptionQueueResponse"));
        objectMapper.registerSubtypes(new NamedType(HttpStatusQueueExceptionQueueResponse.class, "httpStatusQueueExceptionQueueResponse"));
    }

}
```

As we can see on `ErrorQueueResponse` - the concrete type is added into JSON as external property on core level with
name `type`. That `type` value is configured in our `QueueResponseAdvancedJacksonConfig` to let object mapper know what
kind of `ErrorQueueResponse` it's dealing with during serialization / deserialization. If you want to add
new `ErrorQueueResponse` subtypes you have to register them into object mapper this way, or it won't be able to
deserialize them properly.

## Module structure

### Library type modules

#### AMQP Base Model

This module contains

- base `QueueException` and default implementations `InternalQueueException` and `TimeoutQueueException`
- configuration to register these 2 exception implementations as subtypes in `ObjectMapper`
- base `ErrorQueueResponse` and default implementations `InternalExceptionQueueResponse`
  and `TimeoutExceptionQueueResponse`

This module should not be extended at all, just use these classes and package scan for `QueueResponseBaseJacksonConfig`.

#### Error Handler

This module contains

- `RabbitTemplateTimeoutHandler` - aspect to handle detection of queue reply timeouts and throw exception if they occur
- `CustomRabbitListenerErrorHandler` - to handle exceptions inside `@RabbitListener` methods, translate them into
  some `ErrorQueueResponse` and send back to caller
    - `ErrorQueueResponseFactory` and it's base implementations - to translate exceptions into
      serializable `ErrorQueueResponse`-s to send back to caller
- `QueueExceptionMessagePostProcessor` - to handle receiving `ErrorQueueResponse` instead of desired response type and
  translate it into exception inside client
    - `QueueExceptionFactory` and it's base implementations - to translate `ErrorQueueResponse`-s into `QueueException`
      -s
- `RestControllerQueueExceptionHandler` - controller advice to handle uncaught QueueException and build response
  entities out of them
    - `QueueExceptionResponseEntityFactory` and it's base implementations - to translate `QueueException`-s
      into `ResponseEntity`-ies

This module should not be extended at all, just use these classes and register them as beans as needed inside your
configuration classes.

### Project implementation type modules

#### Common DTO

This module contains classes shared between client and server. If you implement bigger projects with more microservices
you'll most likely have split this module into multiple smaller ones.  
In our example it contains

- configuration of AMQP
    - configuration of element names inside `AmqpNames.yml`
    - amqp names access classes e.g. `CalculationAmqpConfig`
    - registration of custom `ErrorQueueResponse`-s inside `QueueResponseAdvancedJacksonConfig`
- dto classes
    - `ErrorQueueResponse` implementations e.g. `DemoAccessQueueExceptionQueueResponse`
    - requests for services e.g. `CalculateSumRequest`
    - responses from services e.g. `PossiblePositivePartsResponse`
- exceptions
    - custom exceptions e.g. `DemoAccessException`
    - custom `QueueException`-s e.h. `DemoAccessQueueException`
- factories to translate
    - exceptions into error messages e.g. `DemoAccessQueueExceptionFactory`
    - error responses into exceptions e.g. `DemoAccessQueueExceptionQueueResponseFactory`
    - exceptions into response entities e.g. `DemoAccessQueueExceptionResponseEntityFactory`

#### Client

This module contains classes specifically required by our client application.
In our example it contains

- API over rest controllers e.g. `DemoCalculationController`
- configuration
    - registration of required AMQP elements inside `DemoAmqpConfig`
    - registration of required handlers and aspects inside `DemoAmqpHandlersConfig`
        - base factories are package scanned by package names `com.dreamap.rabbitmqerrorhandler.factory`
          and `com.dreamap.commons.factory`
    - registration of core `ObjectMapper` and `MessageConverter` inside `DemoBasicConfig`
        - this class also package scans classes with configuration in commons `com.dreamap.commons.config` and in base
          model `com.dreamap.amqp.model.config`
        - it includes registering of `ErrorQueueResponse`-s subtypes by package scans
- services
    - clients to call external services e.g. `DemoCalculationRabbitClient`

#### Server

This module contains classes specifically required by our server application.
In our example it contains

- "API" over RabbitMq as queue listeners e.g. `DemoCalculationAmqpController`
- configuration
    - registration of required AMQP elements inside various classes e.g. `DemoCalculationAmqpConfig`
    - registration of required handlers inside `DemoAmqpHandlersConfig`
        - base factories are package scanned by package names `com.dreamap.rabbitmqerrorhandler.factory`
          and `com.dreamap.commons.factory`
    - registration of core `ObjectMapper` and `MessageConverter` inside `DemoBasicConfig`
        - this class also package scans classes with configuration in commons `com.dreamap.commons.config` and in base
          model `com.dreamap.amqp.model.config`
        - it includes registering of `ErrorQueueResponse`-s subtypes by package scans
- services
    - classes used inside our controller listener methods e.g. `Calculator`

### Usage / extension

#### Add new api method

1. Extend amqp configuration
    1. Extend entries inside `AmqpNames.yml` or create additional configuration file and add with `@PropertySource`
    2. Create a class to easily access these new properties
    3. Add required AMQP element registrations like exchange, queue, binding. Keep in mind that server needs to register
       all elements, while client only need to register exchanges and have access to routing key or queue name.
2. Add listener for your queue
    1. Remember to add error handler if you want to send error queue responses back to caller
3. Inside client application extend some existing rabbit client or implement new one for easy calls to external service

#### Add support for new exception

1. Create new `ErrorQueueResponse` that will be sent back when exception occurs (skip if you reuse existing response)
    1. Register this `ErrorQueueResponse` subtype (see `QueueResponseAdvancedJacksonConfig`)
2. Handle exception inside rabbit listener method and send it as some `ErrorQueueResponse`
    1. Implement `ErrorQueueResponseFactory` that supports required exception type and builds your
       desired `ErrorQueueResponse`
    2. Register this factory as a bean in spring context either by `@Component` on implementation with package scan or
       declaring bean of that type inside some configuration class
3. Handle receiving new ErrorQueueResponse
    1. Implement `QueueExceptionFactory` that supports required `ErrorQueueResponse` and builds proper exception. It
       may, but it doesn't have to be the same exception that `ErrorQueueResponse` was built from
    2. Register this factory as a bean in spring context either by `@Component` on implementation with package scan or
       declaring bean of that type inside some configuration class
4. Handle receiving `QueueException` inside rest controller methods
    1. Implement `QueueExceptionResponseEntityFactory` that supports required `QueueException` and build
       desired `ResponseEntity` out of this exception
    2. Register this factory as a bean in spring context either by `@Component` on implementation with package scan or
       declaring bean of that type inside some configuration class

### Notes

1. To see example of sending request with empty response body (just get success or fail) see message storage store
   message example
2. To see example of sending request with empty request (simple GET) see message storage get messages example
3. To see examples of handling different errors check `DemoErrorAmqpController` and `DemoErrorRabbitClient`

## Implementing processing chain

### Example explanation

This processing chain will be build from several services

- `car-info-service` that loads up extra information about the requested car like production year or salon price
- `parts-info-service` that loads up parts that vehicle is using
- `upgrade-planner` that loads up possible part replacements
- `reminders-manager` that sets up reminders with their date-time and message

The `car-info-service` is our facade that exposes REST API and the other parts are services with rabbit listeners.

### Project pre-set up

To set up our implementation we start with creating empty maven modules

- `demo-rabbitmq-car-info-service`
- `demo-rabbitmq-parts-info-service`
- `demo-rabbitmq-upgrade-planner`
- `demo-rabbitmq-reminders-manager`

...and extra modules that are shared across above modules as dependencies

- `demo-rabbitmq-processing-chain-commons`
- `demo-rabbitmq-processing-chain-client`

Then we add AMQP configuration inside `demo-rabbitmq-processing-chain-commons`

- `demo-rabbitmq-processing-chain/demo-rabbitmq-processing-chain-commons/src/main/java/com/dreamap/YamlPropertySourceFactory.java`
  to enable loading yaml configuration files
- `demo-rabbitmq-processing-chain/demo-rabbitmq-processing-chain-commons/src/main/resources/AmqpNames.yml` containing
  AMQP element names
- `demo-rabbitmq-processing-chain/demo-rabbitmq-processing-chain-commons/src/main/java/com/dreamap/config/CarInfoAmqpConfig.java`
  providing easy access to amqp properties names

Finally, we implement DTO classes inside `demo-rabbitmq-processing-chain-commons`
in `demo-rabbitmq-processing-chain/demo-rabbitmq-processing-chain-commons/src/main/java/com/dreamap/dto` directory.

### Implementing rabbit client

First, to access amqp properties names inside `demo-rabbitmq-processing-chain-client` we add dependency
on `demo-rabbitmq-processing-chain-commons`.  
After that we start implementing methods
inside `demo-rabbitmq-processing-chain/demo-rabbitmq-processing-chain-client/src/main/java/com/dreamap/CarInfoRabbitClient.java`
to easily run each step of processing chain.

We start with the first step which is sending the first message to `parts-info-service` with set up `reply-to` message
property.

```java
public class CarInfoRabbitClient {

    public CarInfo loadFullCarInfo(CarInfo partialCarInfo) {
        AnonymousQueue responseQueue = new AnonymousQueue();
        String responseQueueName = rabbitAdmin.declareQueue(responseQueue);

        if (ObjectUtils.isEmpty(responseQueueName)) {
            throw new IllegalStateException("RabbitMQ broker returned empty queue name");
        }

        try {
            runLoadPartsStep(partialCarInfo, responseQueueName);
            return rabbitTemplate.receiveAndConvert(responseQueueName, receiveTimeout.toMillis(), new ParameterizedTypeReference<>() {});
        } finally {
            rabbitAdmin.deleteQueue(responseQueueName);
        }
    }

}
```

```java
public class CarInfoRabbitClient {

    public void runLoadPartsStep(CarInfo carInfo, String replyToQueue) {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setReplyTo(replyToQueue);

        Message message = rabbitTemplate.getMessageConverter().toMessage(carInfo, messageProperties);
        rabbitTemplate.send(carInfoAmqpConfig.getCarInfoExchangeName(), carInfoAmqpConfig.getLoadPartsQueueName(), message);
    }

}
```

As you can see in the example above in the first step we use `RabbitAdmin` to declare an `AnonymousQueue` and use its
name as reply-to queue that we will listen on.    
We trigger sending message inside `runLoadPartsStep` method, creating `MessageProperties` and setting up `reply-to`
property. Note that in order to add `MessageProperties` into `Message` and send it we must
use `rabbitTemplate.getMessageConverter()` which allows us to serialize our pojo along with `MessageProperties`
After sending the message we start listening on our `AnonymousQueue` for the response - which occurs only in the first
microservice, the next ones will just perform their logic and send message to next service without waiting for any
response.

Then we implement methods to send message to other microservices without waiting for any response.

```java
public class CarInfoRabbitClient {

    public void runLoadReplacementsStep(CarInfo carInfo, String replyToQueue) {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setReplyTo(replyToQueue);

        Message message = rabbitTemplate.getMessageConverter().toMessage(carInfo, messageProperties);
        rabbitTemplate.send(carInfoAmqpConfig.getCarInfoExchangeName(), carInfoAmqpConfig.getLoadReplacementsQueueName(), message);
    }

}
```

```java
public class CarInfoRabbitClient {

    public void runLoadRemindersStep(CarInfo carInfo, String replyToQueue) {
        MessageProperties messageProperties = new MessageProperties();
        messageProperties.setReplyTo(replyToQueue);

        Message message = rabbitTemplate.getMessageConverter().toMessage(carInfo, messageProperties);
        rabbitTemplate.send(carInfoAmqpConfig.getCarInfoExchangeName(), carInfoAmqpConfig.getLoadRemindersQueueName(), message);
    }

}
```

### Implementing car-info-service

We need to add dependencies on

- `demo-rabbitq-processing-chain-commons`
- `demo-rabbitmq-processing-chain-client`
- `demo-rabbitmq-error-handler`

Then we need to add amqp
configuration `demo-rabbitmq-processing-chain/demo-rabbitmq-car-info-service/src/main/java/com/dreamap/config/AmqpConfig.java`
and declare `carInfoExchange` and `rabbitAdmin`.

```java

@Configuration
@RequiredArgsConstructor
@PropertySource(value = "classpath:AmqpNames.yml", factory = YamlPropertySourceFactory.class)
public class AmqpConfig {
    private final CarInfoAmqpConfig carInfoAmqpConfig;

    @Bean
    public DirectExchange carInfoExchange() {
        return new DirectExchange(carInfoAmqpConfig.getCarInfoExchangeName());
    }

    @Bean
    public RabbitAdmin rabbitAdmin(RabbitTemplate rabbitTemplate) {
        return new RabbitAdmin(rabbitTemplate);
    }
}
```

We also have to register `ObjectMapper` and `MessageConverter`
inside `demo-rabbitmq-processing-chain/demo-rabbitmq-car-info-service/src/main/java/com/dreamap/config/SerializationConfig.java`

```java

@Configuration
public class SerializationConfig {
    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        return objectMapper;
    }

    @Bean
    public MessageConverter rabbitMessageConverter(ObjectMapper objectMapper) {
        return new Jackson2JsonMessageConverter(objectMapper);
    }
}
```

I also registered module `JavaTimeModule` and disabled feature `SerializationFeature.WRITE_DATES_AS_TIMESTAMPS` to use
ISO formatting on LocalDateTime serialization.

When we're done with basic configuration we can add error-handling configuration
inside `demo-rabbitmq-processing-chain/demo-rabbitmq-car-info-service/src/main/java/com/dreamap/config/ErrorHandlingConfig.java`.

```java

@Configuration
@ComponentScan("com.dreamap.rabbitmqerrorhandler.factory")
public class ErrorHandlingConfig {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private QueueExceptionMessagePostProcessor queueExceptionMessagePostProcessor;

    @Bean
    public QueueExceptionMessagePostProcessor queueExceptionMessagePostProcessor(List<QueueExceptionFactory> queueExceptionFactories, ObjectMapper objectMapper) {
        return new QueueExceptionMessagePostProcessor(queueExceptionFactories, objectMapper);
    }

    @Bean
    public RabbitTemplateTimeoutHandler rabbitTemplateTimeoutHandler(@Value("${spring.rabbitmq.template.reply-timeout}") Duration rabbitTemplateReplyTimeout, @Value("${spring.rabbitmq.template.receive-timeout}") Duration rabbitTemplateReceiveTimeout) {
        return new RabbitTemplateTimeoutHandler(rabbitTemplateReplyTimeout, rabbitTemplateReceiveTimeout);
    }

    @PostConstruct
    public void configureAfterReceiveQueueExceptionPostProcessor() {
        rabbitTemplate.addAfterReceivePostProcessors(queueExceptionMessagePostProcessor);
    }

}
```

Here we

- use `@ComponentScan("com.dreamap.rabbitmqerrorhandler.factory")` to register all basic factories for timeouts and
  internal errors
- declare `QueueExceptionMessagePostProcessor` to detect if some `ErrorQueueResponse` was returned instead of our
  desired response and change it into exception
- declare `RabbitTemplateTimeoutHandler` to detect timeouts
- configure `afterReceivePostProcessors` to attach `queueExceptiomMessagePostProcessor` to `rabbitTemplate`

The `RestControllerQueueExceptionHandler` is registered automatically as it's in our dependency and
contains `@ControllerAdvice` annotation.

After we're done with configuration we can implement the actual service.
We'll do it by creating a controller like this

```java

@Log4j2
@RestController
@RequiredArgsConstructor
public class CarInfoController {
    private final CarInfoLoader carInfoLoader;
    private final CarInfoRabbitClient carInfoRabbitClient;

    @GetMapping(path = "/car/{carBrand}/{carModel}/info")
    public CarInfo getCarInfo(@PathVariable("carBrand") String carBrand, @PathVariable("carModel") String carModel) {
        CarInfo carInfo = new CarInfo();
        carInfo.setCarBrand(carBrand);
        carInfo.setCarModel(carModel);

        Optional<Integer> productionYear = carInfoLoader.getProductionYear(carBrand, carModel);
        productionYear.ifPresent(carInfo::setProductionYear);

        Optional<BigDecimal> salonPrice = carInfoLoader.getSalonPrice(carBrand, carModel);
        salonPrice.ifPresent(carInfo::setSalonPrice);

        CarInfo fullCarInfo = carInfoRabbitClient.loadFullCarInfo(carInfo);

        log.info("Loaded car info for {} - {}", carBrand, carModel);

        return fullCarInfo;
    }
}
```

and service class like this

```java

@Service
public class CarInfoLoader {
    public Optional<Integer> getProductionYear(String carBrand, String carModel) {
        if (carBrand.equalsIgnoreCase("volkswagen") && carModel.equalsIgnoreCase("polo")) {
            return Optional.of(1975);
        } else {
            return Optional.empty();
        }
    }

    public Optional<BigDecimal> getSalonPrice(String carBrand, String carModel) {
        if (carBrand.equalsIgnoreCase("volkswagen") && carModel.equalsIgnoreCase("polo")) {
            return Optional.of(new BigDecimal(100000));
        } else {
            return Optional.empty();
        }
    }
}
```

You should notice that we use `CarInfo fullCarInfo = carInfoRabbitClient.loadFullCarInfo(carInfo)` as it's the only
method inside our rabbit client that waits for and returns the response.  
The service class is just mocked to return some data for input `"carBrand": "volkswagen", "carModel": "polo"`.

### Implementing parts-info-service

Here we will have many similarities with previous module, but this time it's a little simpler.
Once again we need to add dependencies
on `demo-rabbitmq-processing-chain-commons`, `demo-rabbitmq-processing-chain-client` and `demo-rabbitmq-error-handler`.

Then we add required amqp config
in `demo-rabbitmq-processing-chain/demo-rabbitmq-parts-info-service/src/main/java/com/dreamap/config/AmqpConfig.java`

```java

@Configuration
@RequiredArgsConstructor
public class AmqpConfig {
    private final CarInfoAmqpConfig carInfoAmqpConfig;

    @Bean
    public DirectExchange carInfoExchange() {
        return new DirectExchange(carInfoAmqpConfig.getCarInfoExchangeName());
    }

    @Bean
    public Queue loadPartsQueue() {
        return new Queue(carInfoAmqpConfig.getLoadPartsQueueName());
    }

    @Bean
    public Binding loadPartsQueueBinding(Queue loadPartsQueue, DirectExchange carInfoExchange) {
        return BindingBuilder.bind(loadPartsQueue).to(carInfoExchange).withQueueName();
    }

    @Bean
    public RabbitAdmin rabbitAdmin(RabbitTemplate rabbitTemplate) {
        return new RabbitAdmin(rabbitTemplate);
    }

}
```

The difference here is that we have to declare queue we'll use and the binding for it.

Serialization configuration looks exactly the same.

And in error handling
configuration `demo-rabbitmq-processing-chain/demo-rabbitmq-parts-info-service/src/main/java/com/dreamap/config/ErrorHandlingConfig.java`
we can skip `QueueExceptionMessagePostProcessor` as it affects received responses that we don't have here as well
as `RabbitTemplateTimeoutHandler`.  
The only thing we got to declare here is `CustomRabbitListenerErrorHandler` to catch exceptions and send error
responses.

```java

@Configuration
public class ErrorHandlingConfig {

    @Bean
    public CustomRabbitListenerErrorHandler customRabbitListenerErrorHandler(List<ErrorQueueResponseFactory> queueResponseFactories) {
        return new CustomRabbitListenerErrorHandler(queueResponseFactories);
    }

}
```

When we're done with configuration we can implement the controller and services classes.

```java

@Component
@RequiredArgsConstructor
public class PartsLoaderController {
    private final CarInfoRabbitClient carInfoRabbitClient;
    private final PartsManager partsManager;

    @RabbitListener(queues = "${exchange.carInfo.queue.loadParts.name}", errorHandler = "customRabbitListenerErrorHandler")
    public void loadParts(@Payload CarInfo partialCarInfo, Message message) {
        if (partialCarInfo.getCarModel().equals("model-throwExceptionInPartsInfoService")) {
            throw new IllegalStateException("Parts info service illegal state exception");
        } else {
            List<CarPart> carParts = partsManager.getCarParts(partialCarInfo.getCarBrand(), partialCarInfo.getCarModel());
            partialCarInfo.setParts(carParts);

            carInfoRabbitClient.runLoadReplacementsStep(partialCarInfo, message.getMessageProperties().getReplyTo());
        }
    }
}
```

The controller needs to point to `errorHandler` inside `@RabbitListener`.  
You can also see that we have some mocked code to throw `IllegalStateException`
if `"carModel": "model-throwExceptionInPartsInfoServices`.  
This is added to enable forcing error inside this processing step just to show you how handling works.

```java

@Service
public class PartsManager {

    public List<CarPart> getCarParts(String carBrand, String carModel) {
        if (carBrand.equalsIgnoreCase("volkswagen") && carModel.equalsIgnoreCase("polo")) {
            CarPart wheels = new CarPart();
            wheels.setId("wheels-1");
            wheels.setType(CarPartType.WHEELS);
            wheels.setModel("Big wheels 8000");

            EngineCarPart engine = new EngineCarPart("Quick engine 123", 150d, 2.0);
            engine.setId("engine-1");
            engine.setEstimatedLifespan(Period.of(0, 6, 0));


            ClutchCarPart clutch = new ClutchCarPart("Turbo clutch 3", false, false);
            clutch.setId("clutch-1");
            clutch.setEstimatedLifespan(Period.of(1, 6, 0));

            return Arrays.asList(engine, clutch, wheels);
        } else {
            return Collections.emptyList();
        }
    }
}
```

Once again this is just mocked service to return some data for `"carBrand": "volkswagen", "carModel": "polo"`.

### Implementing upgrade-planner

Just as in [implementing parts info service](#implementing-parts-info-service)

- add dependencies
- add required amqp configuration
- add serialization configuration
- add amqp error handling configuration

The only difference is what actual service is doing and how it's mocked.

```java

@Component
@RequiredArgsConstructor
public class ReplaceablePartsLoaderController {
    private final CarInfoRabbitClient carInfoRabbitClient;
    private final ReplaceablePartsManager replaceablePartsManager;

    @RabbitListener(queues = "${exchange.carInfo.queue.loadReplacements.name}", errorHandler = "customRabbitListenerErrorHandler")
    public void loadReplaceableParts(@Payload CarInfo parialCarInfo, Message message) {
        if (parialCarInfo.getCarModel().equals("model-throwExceptionInUpgradePlanner")) {
            throw new IllegalStateException("Upgrade planner illegal state exception");
        } else {
            List<CarPartReplacement> replaceableParts = replaceablePartsManager.getReplaceableParts(parialCarInfo.getParts());
            parialCarInfo.setReplacementParts(replaceableParts);

            carInfoRabbitClient.runLoadRemindersStep(parialCarInfo, message.getMessageProperties().getReplyTo());
        }
    }
}
```

This time to fire an error inside this service we look for car model `model-throwExceptionInUpgradePlanner`.

The service is also performing a different task.

```java

@Service
public class ReplaceablePartsManager {

    public List<CarPartReplacement> getReplaceableParts(List<CarPart> originalParts) {
        return Optional.ofNullable(originalParts).stream().flatMap(Collection::stream)
                .filter(Objects::nonNull)
                .map(this::getReplaceableParts)
                .flatMap(Collection::stream)
                .toList();
    }

    private List<CarPartReplacement> getReplaceableParts(CarPart carPart) {
        if (carPart.getId().equalsIgnoreCase("engine-1")) {
            CarPartReplacement greenEngine = new CarPartReplacement();
            greenEngine.setOriginalPartId(carPart.getId());
            greenEngine.setModel("Green engine");
            greenEngine.setEstimatedLifespan(Period.ofYears(2));

            CarPartReplacement blueEngine = new CarPartReplacement();
            blueEngine.setOriginalPartId(carPart.getId());
            blueEngine.setModel("Blue engine");
            blueEngine.setEstimatedLifespan(Period.ofYears(5));

            return Arrays.asList(greenEngine, blueEngine);
        } else if (carPart.getId().equalsIgnoreCase("clutch-1")) {
            CarPartReplacement greenClutch = new CarPartReplacement();
            greenClutch.setOriginalPartId(carPart.getId());
            greenClutch.setModel("Green clutch");
            greenClutch.setEstimatedLifespan(Period.ofYears(2));

            return Collections.singletonList(greenClutch);
        } else {
            return Collections.emptyList();
        }
    }
}
```

### Implementing reminders-manager

Just as in [implementing parts info service](#implementing-parts-info-service)

- add dependencies
- add required amqp configuration
- add serialization configuration
- add amqp error handling configuration

The only difference is what actual service is doing and how it's mocked.

```java

@Component
@RequiredArgsConstructor
public class RemindersController {
    private final RabbitTemplate rabbitTemplate;
    private final RemindersManager remindersManager;

    @RabbitListener(queues = "${exchange.carInfo.queue.loadReminders.name}", errorHandler = "customRabbitListenerErrorHandler")
    public void loadReminders(@Payload CarInfo partialCarInfo, Message message) {
        if (partialCarInfo.getCarModel().equals("model-throwExceptionInRemindersManager")) {
            throw new IllegalStateException("Reminders manager illegal state exception");
        } else {
            List<Reminder> reminders = remindersManager.generateReminders(partialCarInfo);
            partialCarInfo.setReminders(reminders);

            rabbitTemplate.convertAndSend(message.getMessageProperties().getReplyTo(), partialCarInfo);
        }
    }
}
```

This time to fire an error inside this service we look for car model `model-throwExceptionInRemindersManager`.

The service is also performing a different task.

```java

@Service
public class RemindersManager {
    private static final String REMINDER_MESSAGE_TEMPLATE = "You should check your %s - %s at official service";

    public List<Reminder> generateReminders(CarInfo partialCarInfo) {
        return Optional.ofNullable(partialCarInfo)
                .map(CarInfo::getParts)
                .stream().flatMap(Collection::stream)
                .map(this::buildPartServiceReminder)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .toList();
    }

    public Optional<Reminder> buildPartServiceReminder(CarPart carPart) {
        if (carPart.getEstimatedLifespan() != null && carPart.getModel() != null) {
            LocalDateTime notificationLocalDateTime = LocalDateTime.now().plus(carPart.getEstimatedLifespan()).minus(2, ChronoUnit.WEEKS);

            String formattedMessage = REMINDER_MESSAGE_TEMPLATE.formatted(carPart.getType().toString(), carPart.getModel());

            Reminder reminder = new Reminder();
            reminder.setReminderDate(notificationLocalDateTime);
            reminder.setMessage(formattedMessage);

            return Optional.of(reminder);
        } else {
            return Optional.empty();
        }
    }
}
```

### Extended RabbitTemplateTimeoutHandler to handle receives

To enable timeout detection on `rabbitTemplate.receiveAndConvert(...)` we added another pointcut and advice
in `RabbitTemplateTimeoutHandler`.

```java

@Log4j2
@Aspect
@RequiredArgsConstructor
public class RabbitTemplateTimeoutHandler {
    private final Duration rabbitTemplateReplyTimeout;
    private final Duration rabbitTemplateReceiveTimeout;

    @Pointcut("execution(* org.springframework.amqp.rabbit.core.RabbitTemplate.receiveAndConvert(..))")
    public void receiveAndConvertPointcut() {
        // just a pointcut method
    }

    @Around("receiveAndConvertPointcut()")
    public Object throwExceptionOnReceiveTimeoutNullResponse(ProceedingJoinPoint joinPoint) throws Throwable {
        long timestamp = System.currentTimeMillis();
        Object returnValue = joinPoint.proceed();

        if (returnValue == null && System.currentTimeMillis() - timestamp >= rabbitTemplateReceiveTimeout.toMillis()) {
            log.warn("Could not receive message in time, throwing TimeoutQueueException");
            throw new TimeoutQueueException("Timeout occurred");
        } else {
            return returnValue;
        }
    }

}
```

This extension changed constructor to accept 2 separate durations - one for reply timeout and one for receive timeout.  
Inside our `ErrorHandlingConfig` we used `@Value` to inject these properties without defaults, therefore made them
mandatory.

```java

@Configuration
public class ErrorHandlingConfig {

    @Bean
    public RabbitTemplateTimeoutHandler rabbitTemplateTimeoutHandler(@Value("${spring.rabbitmq.template.reply-timeout}") Duration rabbitTemplateReplyTimeout, @Value("${spring.rabbitmq.template.receive-timeout}") Duration rabbitTemplateReceiveTimeout) {
        return new RabbitTemplateTimeoutHandler(rabbitTemplateReplyTimeout, rabbitTemplateReceiveTimeout);
    }

}
```

Example configuration file can look like this.

```yaml
spring:
  rabbitmq:
    template:
      reply-timeout: PT10S
      receive-timeout: PT10S
```

---

## Future ideas / todo

- reformat document to make it easier to read
    - separate elements
        - conception with diagrams
        - quick steps as reminder
        - detailed how to do it
        - detailed how it works
- add support for AsyncRabbitTemplate error handling
    - should throw proper QueueException on .get()
- describe async processing in tutorial
